import requests
from bs4 import BeautifulSoup
import re
import time

# this works to get level 2, 3, and 4 terms correctly

search_terms = open("C:/Users/scorvene/Desktop/RIS_taxo/RIS level 1.txt", 'rb')
ris_level_below = open("C:/Users/scorvene/Desktop/RIS_taxo/RIS level 2.txt", 'rb')
f = open("C:/Users/scorvene/Desktop/RIS_taxo/numbers2A.txt", 'wb')

url = 'https://www.hbs.edu/faculty/Pages/search.aspx?searchtype=&tp='

terms = list()
for t in search_terms:
    t = t.strip().encode('UTF-8')
    terms.append(t)
# print terms

topics = list()
for subject in ris_level_below:
    subject = subject.strip().encode('UTF-8')
    topics.append(subject)

already_searched = list()

for term in terms:
    print 'working on ' + term
    search = url + term
    # print search

    response = requests.get(search)
    html = response.content
    soup = BeautifulSoup(html, 'lxml')

    list_items = soup.findAll('li')
    for item in list_items:
        if item.text is not None:
            i = item.text.encode('UTF-8')

            if i.find("(") != -1:
                item_list = i.split("(")
                if item_list[0].strip() in topics:
                    if item_list[0].strip not in already_searched:
                        term = item_list[0].strip()
                        already_searched.append(term)
                        # print already_searched
                        # new_i = re.sub('\t', '', i)
                        # final_i = re.sub('\s\s', '', new_i)
                        # final_i_list = final_i.split("(")
                        # term = final_i_list[0].strip()
                        print 'getting results for ' + term
                        amount = re.sub("\)", '', item_list[1])
                        data = term + ';' + amount
                        data = data + '\n'
                        # print data
                        f.write(data)
                    else:
                        pass
print already_searched

