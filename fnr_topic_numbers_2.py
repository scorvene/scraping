import requests
from bs4 import BeautifulSoup
import re
import time

ris = open("C:/Users/scorvene/Desktop/RIS_taxo/RIS level 1A.txt", 'rb')
ris_level_1 = open("C:/Users/scorvene/Desktop/RIS_taxo/RIS level 1.txt", 'rb')
f = open("C:/Users/scorvene/Desktop/RIS_taxo/numbers1.txt", 'wb')

url = 'https://www.hbs.edu/faculty/Pages/search.aspx?searchtype=&tp='

topics = list()
level_1_topics = list()

for t in ris:
    t = t.strip().encode('UTF-8')
    topics.append(t)

for top in ris_level_1:
    top = top.strip().encode('UTF-8')
    level_1_topics.append(top)

counter = 0
unused = list()
for topic in topics:
    counter += 1
    print 'working in line ' + str(counter)
    search = url + topic
    # print search
    response = requests.get(search)
    html = response.content
    soup = BeautifulSoup(html, 'lxml')

    list_items = soup.findAll('li')
    for item in list_items:
        if item.text is not None:
            i = item.text.encode('UTF-8')

            if i.find("(") != -1:
                item_list = i.split("(")
                if item_list[0].strip() in level_1_topics:
                    new_i = re.sub('\t', '', i)
                    final_i = re.sub('\s\s', '', new_i)
                    final_i_list = final_i.split("(")
                    term = final_i_list[0].strip()
                    # print 'working on ' + term
                    amount = re.sub("\)", '', final_i_list[1])
                    data = term + ';' + amount
                    data = data + '\n'
                    f.write(data)
                elif i in topics:
                    new_i = re.sub('\t', '', i)
                    final_i = re.sub('\s\s', '', new_i)
                    final_i_list = final_i.split("(")
                    term = final_i_list[0].strip()
                    # print 'working on ' + term
                    amount = re.sub("\)", '', final_i_list[1])
                    data = term + ';' + amount
                    data = data + '\n'
                    f.write(data)
#             elif i in topics:
#                 if i not in unused:
#                     unused.append(i)
#
# for n in unused:
#     n = n + ';0\n'
#     f.write(n)
