# -*- coding: UTF-8 -*-

import openpyxl as xl
from openpyxl import Workbook
import argparse

parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
# parser.add_argument('--output-file', dest='out_file', required=True)
parser.add_argument('--pen-name-input-file', dest='pen_file', required=True)
parser.add_argument('--pen-sheet-name', dest='pen_sheet_name', required=True)
parser.add_argument('--fnr-input-file', dest='fnr_file', required=True)
parser.add_argument('--fnr-sheet-name', dest='fnr_sheet_name', required=True)
parser.add_argument('--thumb-input-file', dest='thumb_file', required=True)
parser.add_argument('--thumb-sheet-name', dest='thumb_sheet_name', required=True)
arguments = parser.parse_args()

# outputFile = arguments.out_file

penNameFile = arguments.pen_file
penSheet = arguments.pen_sheet_name

fnrFacultyFile = arguments.fnr_file
fnrSheet = arguments.fnr_sheet_name

thumbFile = arguments.thumb_file
thumbSheet = arguments.thumb_sheet_name

pen_wb = xl.load_workbook(penNameFile)
pen_sheet = pen_wb[penSheet]

pen_dict = dict()
pen_rows = pen_sheet.max_row + 1
for n in range(2, pen_rows):
    faculty_id_cell = 'A' + str(n)
    faculty_id = str(pen_sheet[faculty_id_cell].value)

    pref_name_cell = 'B' + str(n)
    pref_name = pen_sheet[pref_name_cell].value.encode('UTF-8')
    pen_dict[faculty_id] = [pref_name]

    pen_cell = 'C' + str(n)
    pen_names = pen_sheet[pen_cell].value.encode('UTF-8')
    pen_dict[faculty_id].append(pen_names)
# print pen_dict


fnr_wb = xl.load_workbook(fnrFacultyFile)
fnr_sheet = fnr_wb[fnrSheet]

fnr_dict = dict()
fnr_rows = fnr_sheet.max_row + 1
for nn in range(2, fnr_rows):
    fnr_faculty_id_cell = 'A' + str(nn)
    fnr_faculty_id = str(fnr_sheet[fnr_faculty_id_cell].value)

    name_cell = 'B' + str(nn)
    name = str(fnr_sheet[name_cell].value)
    fnr_dict[fnr_faculty_id] = [name]

    title_cell = 'C' + str(nn)
    title = fnr_sheet[title_cell].value.encode('UTF-8')
    fnr_dict[fnr_faculty_id].append(title)

    profile_cell = 'D' + str(nn)
    profile = fnr_sheet[profile_cell].value.encode('UTF-8')
    fnr_dict[fnr_faculty_id].append(profile)

    status_cell = 'E' + str(nn)
    status = fnr_sheet[status_cell].value.encode('UTF-8')
    fnr_dict[fnr_faculty_id].append(status)
# print fnr_dict


thumb_wb = xl.load_workbook(thumbFile)
thumb_sheet = thumb_wb[thumbSheet]

thumb_dict = dict()
thumb_rows = thumb_sheet.max_row + 1

for nnn in range(2, thumb_rows):
    thumb_faculty_id_cell = 'A' + str(nnn)
    thumb_faculty_id = str(thumb_sheet[thumb_faculty_id_cell].value)

    unit_cell = 'C' + str(nnn)
    unit = thumb_sheet[unit_cell].value.encode('UTF-8')
    thumb_dict[thumb_faculty_id] = [unit]

    thumb_cell = 'D' + str(nnn)
    thumb = thumb_sheet[thumb_cell].value.encode('UTF-8')
    thumb_dict[thumb_faculty_id].append(thumb)

    bio_cell = 'E' + str(nnn)
    bio = thumb_sheet[bio_cell].value.encode('UTF-8')
    thumb_dict[thumb_faculty_id].append(bio)
# print thumb_dict

wb = Workbook()
wb_name = 'faculty_test.xlsx'
sn = wb.active
sn.title = 'faculty'

headers = ['hbs-faculty-schema-v3:HBSFacultyId', 'Name', 'altLabel', 'hbs-faculty-schema-v3:HBSTitle',
           'hbs-faculty-schema-v3:HBSProfilePage', 'hbs-faculty-schema-v3:HBSFacultyUnit',
           'hbs-faculty-schema-v3:HBSFacultyStatus', 'hbs-faculty-schema-v3:HBSFacultyThumbNail',
           'hbs-faculty-schema-v3:Description', 'hbs-faculty-schema-v3:DateOfBirth',
           'hbs-faculty-schema-v3:PlaceOfBirth', 'hbs-faculty-schema-v3:DBpediaResourceURI',
           'hbs-faculty-schema-v3:DBpediaPageURI', 'hbs-faculty-schema-v3:DBPediaSubtitle']

columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']

for entry in range(14):
    header_address = columns[entry] + '1'
    sn[header_address] = headers[entry]

counter = 1
for item in fnr_dict:
    counter += 1

    id_address = 'A' + str(counter)
    name_address = 'B' + str(counter)
    title_address = 'D' + str(counter)
    status_address = 'G' + str(counter)
    pen_address = 'C' + str(counter)
    unit_address = 'F' + str(counter)
    profile_address = 'E' + str(counter)
    thumbnail_address = 'H' + str(counter)
    bio_address = 'I' + str(counter)

    sn[id_address] = int(item)
    sn[name_address] = fnr_dict[item][0]
    sn[title_address] = fnr_dict[item][1]
    sn[profile_address] = fnr_dict[item][2]
    sn[status_address] = fnr_dict[item][3]
    if item in pen_dict:
        pen_list = ';'.join(pen_dict[item])
        # print pen_list
        sn[pen_address] = pen_list
    if item in thumb_dict:
        sn[unit_address] = thumb_dict[item][0]
        sn[thumbnail_address] = thumb_dict[item][1]
        sn[bio_address] = thumb_dict[item][2]

wb.save(filename=wb_name)
