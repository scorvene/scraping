august = ['Rawi E. Abdelal', 'David Ager', 'Juan Alcacer', 'Laura Alfaro', 'Jose B. Alvarez', 'Teresa M. Amabile', 'Tomomichi Amano',
          'Bharat N. Anand', 'Lynda M. Applegate', 'Eva Ascarza', 'Julia B. Austin', 'Jill J. Avery',
          'Joseph L. Badaracco', 'Malcolm P. Baker', 'Kate Barasz', 'Peter Barrett', 'John F Batter',
          'Julie Battilana', 'Max H. Bazerman', 'Sven Beckert', 'David E. Bell', 'Edward B. Berk',
          'Ethan S. Bernstein', 'John Beshears', 'Alison Wood Brooks', 'Ryan W. Buell', 'Jeffrey Bussgang',
          'Timothy Butler', 'Dennis Campbell', 'Ramon Casadesus-Masanell', 'Alberto F. Cavallo', 'Frank V. Cespedes',
          'Amitabh Chandra', 'Kyle Chauvin', 'Prithwiraj Choudhury', 'Clayton M. Christensen', 'Michael Chu',
          'Doug J. Chung', 'Katherine B. Coffman', 'Lauren H. Cohen', 'Randolph B. Cohen', 'Shawn A. Cole',
          'David J. Collis', 'Joshua D. Coval', 'Martha J. Crawford', 'Zoe B. Cullen', 'H. Lawrence Culp',
          'Leemore S. Dafny', 'Srikant M. Datar', 'Antonio Davila', 'Thomas J. DeLong', 'Mihir A. Desai',
          'Rohit Deshpande', 'Aiyesha Dey', 'Marco Di Maggio', 'Rafael M. Di Tella', 'John D. Dionne',
          'Robert J. Dolan', 'James J. Dowd', 'Benjamin G. Edelman', 'Amy C. Edmondson', 'Mark L. Egan',
          'Thomas R. Eisenmann', 'Anita Elberse', 'Caroline M. Elkins', 'Robin J. Ely', 'Willis M. Emmons',
          'Benjamin C. Esty', 'Christine L. Exley', 'Kristin E. Fabbe', 'Chiara Farronato', 'Thomas W. Feeley',
          'Kris Johnson Ferreira', 'Trevor Fetter', 'Mattias E. Fibiger', 'Sara L. Fleiss', 'C. Fritz Foley',
          'Frances X. Frei', 'Jeremy S. Friedman', 'Walter A. Friedman', 'David G. Fubini', 'Joseph B. Fuller',
          'Tristan Gagnon-Bartsch', 'Susanna Gallani', 'Vikram S. Gandhi', 'William W. George', 'Nori Gerardo Lietz',
          'Shikhar Ghosh', 'Stuart C. Gilson', 'Francesca Gino', 'Kathy E. Giusti', 'Joel Goh', 'Lena G. Goldberg',
          'Paul A. Gompers', 'John T. Gourville', 'Daniel W. Green', 'Jerry R. Green', 'Shane Greenstein',
          'Robin Greenwood', 'Daniel P. Gross', 'Allen S. Grossman', 'Boris Groysberg', 'Yael Grushka-Cockayne',
          'Ranjay Gulati', 'Sunil Gupta', 'Brian J. Hall', 'Richard G. Hamermesh', 'Janice H. Hammond',
          'Samuel G. Hanson', 'Paul M. Healy', 'Jonas Heese', 'Rebecca M. Henderson', 'Regina E. Herzlinger',
          'Robert F. Higgins', 'Linda A. Hill', 'Nien-he Hsieh', 'Laura Huang', 'Chester A. Huber', 'Robert S. Huckman',
          'Reshmaan N. Hussam', 'Marco Iansiti', 'Ayelet Israeli', 'Victoria Ivashina', 'Srikanth Jagabathula',
          'Leslie K. John', 'Geoffrey G. Jones', 'Rosabeth M. Kanter', 'Robert S. Kaplan', 'Huseyin S. Karaca',
          'Stephen P. Kaufman', 'Elizabeth A. Keenan', 'Anat Keinan', 'William R. Kerr', 'W. Carl Kester',
          'Tarun Khanna', 'Rakesh Khurana', 'John Jong-Hyun Kim', 'William C. Kirby', 'Nancy F. Koehn',
          'Elon Kohlberg', 'Scott Duke Kominers', 'Rembrand M. Koning', 'Mark R. Kramer', 'Joshua Lev Krieger',
          'Karim R. Lakhani', 'Rajiv Lal', 'Joseph B. Lassiter', 'Herman B. Leonard', 'Josh Lerner', 'Jay W. Lorsch',
          'Gary W. Loveman', 'Michael Luca', 'Hong Luo', 'Alan D. MacCormack', 'Alexander J. MacKay',
          'John D. Macomber', 'Deepak Malhotra', 'Christopher J. Malloy', 'Joshua D. Margolis', 'E. Scott Mayfield',
          'Anthony Mayo', 'Emily R. McComb', 'Rory M. McDonald', 'Henry W. McGee', 'Kathleen L. McGinn',
          'Paul D. McKinnon', 'Karen Mills', 'Allison H. Mnookin', 'Kevin P. Mohan', 'Navid Mojir',
          'Cynthia A. Montgomery', 'Youngme Moon', 'Antonio Moreno', 'David A. Moss', 'Kristin Williams Mugford',
          'Kyle R. Myers', 'Frank Nagle', 'Ashish Nanda', 'Ramana Nanda', 'V.G. Narayanan', 'Das Narayandas',
          'Tsedal Neeley', 'Trung Nguyen', 'Donald K. Ngwe', 'Tom Nicholas', 'Nitin Nohria', 'Michael I. Norton',
          'Felix Oberholzer-Gee', 'Elie Ofek', 'Lynn S. Paine', 'Krishna G. Palepu', 'Gerardo Perez Cavazos',
          'Leslie A. Perlow', 'Caspar David Peter', 'Gary P. Pisano', 'Jeffrey T. Polzer', 'Vincent Pons',
          'Michael E. Porter', 'Matthew Rabin', 'Ryan L. Raffaelli', 'Ananth Raman', 'Karthik Ramanna',
          'Lakshmi Ramarajan', 'Jorge Ramirez-Vallejo', 'V. Kasturi Rangan', 'Jeffrey F. Rayport',
          'Sophus A. Reinert', 'Forest L. Reinhardt', 'George A. Riedel', 'Natalia Rigol', 'Meg Rithmire',
          'Jan W. Rivkin', 'Mark N. Roberge', 'Steven S. Rogers', 'Dante Roscini', 'Benjamin N. Roth',
          'Ethan C. Rouen', 'Richard S. Ruback', 'Raffaella Sadun', 'William A. Sahlman', 'Tatiana Sandino',
          'Shelle M. Santana', 'Laura Phillips Sawyer', 'David S. Scharfstein', 'Leonard A. Schlesinger',
          'Amy W. Schulman', 'Joshua R. Schwartzstein', 'James K. Sebenius', 'Arthur I Segel', 'George Serafeim',
          'Kevin W. Sharer', 'Sudev J Sheth', 'Willy C. Shih', 'Andrei Shleifer', 'Anywhere Sikochi', 'Robert Simons',
          'Kristin L. Sippl', 'Emil N. Siriwardane', 'Mario Small', 'Scott A. Snook', 'Eugene F. Soltes',
          'Debora L. Spar', 'Suraj Srinivasan', 'Erik Stafford', 'Christopher T. Stanton', 'Ariel D. Stern',
          'Guhan Subramanian', 'Sandra J. Sucher', 'Adi Sunderam', 'Marco E. Tabellini', 'Hirotaka Takeuchi',
          'Jorge Tamayo', 'Thales S. Teixeira', 'Stefan H. Thomke', 'Monique Burns Thompson', 'Michael W. Toffel',
          'Brian L Trelstad', 'Gunnar Trumbull', 'Chinmay Tumbe', 'Michael L. Tushman', 'Boris Vallee',
          'Derek C. M. van Bever', 'Eric J. Van den Steen', 'Luis M. Viceira', 'Richard H.K. Vietor',
          'Charles C.Y. Wang', 'Andrew Wasynczuk', 'Matthew C. Weinzierl', 'Mitchell B. Weiss', 'John R. Wells', 'Ashley V. Whillans', 'Robert F. White', 'Emily Williams', 'Christina R. Wing', 'Andy Wu',
          'Charles F. Wu', 'Dennis A. Yao', 'Michael H. Yeomans', 'David B. Yoffie', 'Sid Yog', 'Royce G. Yudkoff', 'Andy Zelleke', 'Letian Zhang', 'Ting Zhang', 'Feng Zhu', 'Julian J. Zlatev',
          'James E. Austin', 'Carliss Y. Baldwin', 'Christopher A. Bartlett', 'Michael Beer', 'H. Kent Bowen', 'Joseph L. Bower', 'Stephen P. Bradley', 'William J. Bruns', 'James I. Cash', 'Kim B. Clark',
          'Dwight B. Crane', 'M. Colyer Crum', 'John A. Deighton', 'J. Ronald Fox', 'Kenneth A. Froot', 'William E. Fruhan', 'John J. Gabarro', 'Ray A. Goldberg', 'Stephen A. Greyser', 'Myra M. Hart',
          'David F. Hawkins', 'Robert H. Hayes', 'Samuel L. Hayes', 'James L. Heskett', 'Michael C. Jensen', 'John P. Kotter', 'Dorothy A. Leonard', 'Jay O. Light', 'George C. Lodge', 'Paul W. Marshall',
          'John H. McArthur', 'F. Warren McFarlan', 'Robert C. Merton', 'Richard F. Meyer', 'D. Quinn Mills', 'Richard L. Nolan', 'Andre F. Perold', 'Thomas R. Piper', 'William J. Poorvu', 'John W. Pratt',
          'John A. Quelch', 'Henry B. Reiling', 'Alvin E. Roth', 'Malcolm S. Salter', 'W. Earl Sasser', 'Arthur Schleifer', 'Bruce R. Scott', 'Benson P. Shapiro', 'Roy D. Shapiro', 'Alvin J. Silk',
          'Wickham Skinner', 'Howard H. Stevenson', 'Richard S. Tedlow', 'David A. Thomas', 'Peter Tufano', 'Richard E. Walton', 'Louis T. Wells', 'Michael A. Wheeler', 'Steven C. Wheelwright',
          'Michael Y. Yoshino', 'Gerald Zaltman', 'Shoshana Zuboff']

july = ['Rawi E. Abdelal', 'Noubar B. Afeyan', 'David Ager', 'Juan Alcacer', 'Laura Alfaro', 'Jose B. Alvarez', 'Teresa M. Amabile', 'Tomomichi Amano', 'Bharat N. Anand', 'Lynda M. Applegate', 'Eva Ascarza',
        'Julia B. Austin', 'Jill J. Avery', 'Joseph L. Badaracco', 'Malcolm P. Baker', 'Kate Barasz', 'Peter Barrett', 'John F Batter', 'Julie Battilana', 'Max H. Bazerman', 'Sven Beckert', 'David E. Bell',
        'Edward B. Berk', 'Ethan S. Bernstein', 'John Beshears', 'Alison Wood Brooks', 'Ryan W. Buell', 'Jeffrey Bussgang', 'Timothy Butler', 'Dennis Campbell', 'Ramon Casadesus-Masanell', 'Alberto F. Cavallo',
        'Frank V. Cespedes', 'Amitabh Chandra', 'Kyle Chauvin', 'Prithwiraj Choudhury', 'Clayton M. Christensen', 'Michael Chu', 'Doug J. Chung', 'Katherine B. Coffman', 'Lauren H. Cohen', 'Randolph B. Cohen',
        'Shawn A. Cole', 'David J. Collis', 'Joshua D. Coval', 'Martha J. Crawford', 'Zoe B. Cullen', 'H. Lawrence Culp', 'Leemore S. Dafny', 'Srikant M. Datar', 'Antonio Davila', 'Thomas J. DeLong',
        'Mihir A. Desai', 'Rohit Deshpande', 'Aiyesha Dey', 'Marco Di Maggio', 'Rafael M. Di Tella', 'John D. Dionne', 'Robert J. Dolan', 'James J. Dowd', 'Benjamin G. Edelman', 'Amy C. Edmondson',
        'Mark L. Egan', 'Thomas R. Eisenmann', 'Anita Elberse', 'Caroline M. Elkins', 'Robin J. Ely', 'Willis M. Emmons', 'Benjamin C. Esty', 'Christine L. Exley', 'Kristin E. Fabbe', 'Chiara Farronato',
        'Thomas W. Feeley', 'Kris Johnson Ferreira', 'Mattias E Fibiger', 'Sara L. Fleiss', 'C. Fritz Foley', 'Frances X. Frei', 'Jeremy S. Friedman', 'Walter A. Friedman', 'David G. Fubini', 'Joseph B. Fuller',
        'Tristan Gagnon-Bartsch', 'Susanna Gallani', 'Vikram S. Gandhi', 'William W. George', 'Nori Gerardo Lietz', 'Shikhar Ghosh', 'Stuart C. Gilson', 'Francesca Gino', 'Kathy E. Giusti', 'Joel Goh',
        'Lena G. Goldberg', 'Paul A. Gompers', 'John T. Gourville', 'Daniel W Green', 'Jerry R. Green', 'Shane Greenstein', 'Robin Greenwood', 'Daniel P. Gross', 'Allen S. Grossman', 'Boris Groysberg',
        'Yael Grushka-Cockayne', 'Ranjay Gulati', 'Sunil Gupta', 'Brian J. Hall', 'Richard G. Hamermesh', 'Janice H. Hammond', 'Samuel G. Hanson', 'Paul M. Healy', 'Jonas Heese', 'Rebecca M. Henderson',
        'Regina E. Herzlinger', 'Robert F. Higgins', 'Linda A. Hill', 'Nien-he Hsieh', 'Laura Huang', 'Chester A. Huber', 'Robert S. Huckman', 'Reshmaan N. Hussam', 'Marco Iansiti', 'Ayelet Israeli',
        'Victoria Ivashina', 'Srikanth Jagabathula', 'Leslie K. John', 'Geoffrey G. Jones', 'Rosabeth M. Kanter', 'Robert S. Kaplan', 'Huseyin S. Karaca', 'Stephen P. Kaufman', 'Elizabeth A. Keenan',
        'Anat Keinan', 'William R. Kerr', 'W. Carl Kester', 'Tarun Khanna', 'Rakesh Khurana', 'John Jong-Hyun Kim', 'William C. Kirby', 'Nancy F. Koehn', 'Elon Kohlberg', 'Scott Duke Kominers',
        'Rembrand M. Koning', 'Mark R. Kramer', 'Joshua Lev Krieger', 'Karim R. Lakhani', 'Rajiv Lal', 'Joseph B. Lassiter', 'Herman B. Leonard', 'Josh Lerner', 'Jay W. Lorsch', 'Gary W. Loveman',
        'Michael Luca', 'Hong Luo', 'Alan D. MacCormack', 'Alexander J. MacKay', 'John D. Macomber', 'Deepak Malhotra', 'Christopher J. Malloy', 'Joshua D. Margolis', 'E. Scott Mayfield', 'Anthony Mayo',
        'Emily R. McComb', 'Rory M. McDonald', 'Henry W. McGee', 'Kathleen L. McGinn', 'Paul D. McKinnon', 'Karen Mills', 'Allison H. Mnookin', 'Kevin P. Mohan', 'Navid Mojir', 'Cynthia A. Montgomery',
        'Youngme Moon', 'Antonio Moreno', 'David A. Moss', 'Kristin Williams Mugford', 'Kyle Myers', 'Frank Nagle', 'Ashish Nanda', 'Ramana Nanda', 'V.G. Narayanan', 'Das Narayandas', 'Tsedal Neeley',
        'Trung Nguyen', 'Donald K. Ngwe', 'Tom Nicholas', 'Nitin Nohria', 'Michael I. Norton', 'Felix Oberholzer-Gee', 'Elie Ofek', 'Lynn S. Paine', 'Krishna G. Palepu', 'Gerardo Perez Cavazos',
        'Leslie A. Perlow', 'Caspar David Peter', 'Gary P. Pisano', 'Jeffrey T. Polzer', 'Vincent Pons', 'Michael E. Porter', 'Matthew Rabin', 'Ryan L. Raffaelli', 'Ananth Raman', 'Karthik Ramanna',
        'Lakshmi Ramarajan', 'Jorge Ramirez-Vallejo', 'V. Kasturi Rangan', 'Jeffrey F. Rayport', 'Sophus A. Reinert', 'Forest L. Reinhardt', 'George A. Riedel', 'Natalia Rigol', 'Meg Rithmire',
        'Jan W. Rivkin', 'Mark N. Roberge', 'Steven S. Rogers', 'Dante Roscini', 'Benjamin N. Roth', 'Ethan C. Rouen', 'Richard S. Ruback', 'Raffaella Sadun', 'William A. Sahlman', 'Tatiana Sandino',
        'Shelle M. Santana', 'Laura Phillips Sawyer', 'David S. Scharfstein', 'Leonard A. Schlesinger', 'Amy W. Schulman', 'Joshua R. Schwartzstein', 'James K. Sebenius', 'Arthur I Segel', 'George Serafeim',
        'Kevin W. Sharer', 'Sudev J Sheth', 'Willy C. Shih', 'Andrei Shleifer', 'Anywhere Sikochi', 'Robert Simons', 'Kristin L. Sippl', 'Emil N. Siriwardane', 'Mario Small', 'Scott A. Snook',
        'Eugene F. Soltes', 'Debora L. Spar', 'Suraj Srinivasan', 'Erik Stafford', 'Christopher T. Stanton', 'Ariel D. Stern', 'Guhan Subramanian', 'Sandra J. Sucher', 'Adi Sunderam', 'Marco E Tabellini',
        'Hirotaka Takeuchi', 'Jorge Tamayo', 'Thales S. Teixeira', 'Stefan H. Thomke', 'Monique Burns Thompson', 'Michael W. Toffel', 'Brian L Trelstad', 'Gunnar Trumbull', 'Michael L. Tushman', 'Boris Vallee',
        'Derek C. M. van Bever', 'Eric J. Van den Steen', 'Luis M. Viceira', 'Richard H.K. Vietor', 'Charles C.Y. Wang', 'Andrew Wasynczuk', 'Matthew C. Weinzierl', 'Mitchell B. Weiss', 'John R. Wells',
        'Ashley V. Whillans', 'Robert F. White', 'Emily Williams', 'Christina R. Wing', 'Andy Wu', 'Charles F. Wu', 'Dennis A. Yao', 'Michael H Yeomans', 'David B. Yoffie', 'Sid Yog', 'Royce G. Yudkoff',
        'Andy Zelleke', 'Letian Zhang', 'Ting Zhang', 'Feng Zhu', 'Julian J. Zlatev', 'James E. Austin', 'Carliss Y. Baldwin', 'Christopher A. Bartlett', 'Michael Beer', 'H. Kent Bowen', 'Joseph L. Bower',
        'Stephen P. Bradley', 'William J. Bruns', 'James I. Cash', 'Kim B. Clark', 'Dwight B. Crane', 'M. Colyer Crum', 'John A. Deighton', 'J. Ronald Fox', 'Kenneth A. Froot', 'William E. Fruhan',
        'John J. Gabarro', 'Ray A. Goldberg', 'Stephen A. Greyser', 'Myra M. Hart', 'David F. Hawkins', 'Robert H. Hayes', 'Samuel L. Hayes', 'James L. Heskett', 'Michael C. Jensen', 'John P. Kotter',
        'Dorothy A. Leonard', 'Jay O. Light', 'George C. Lodge', 'Paul W. Marshall', 'John H. McArthur', 'F. Warren McFarlan', 'Robert C. Merton', 'Richard F. Meyer', 'D. Quinn Mills', 'Richard L. Nolan',
        'Andre F. Perold', 'Thomas R. Piper', 'William J. Poorvu', 'John W. Pratt', 'John A. Quelch', 'Henry B. Reiling', 'Alvin E. Roth', 'Malcolm S. Salter', 'W. Earl Sasser', 'Arthur Schleifer',
        'Bruce R. Scott', 'Benson P. Shapiro', 'Roy D. Shapiro', 'Alvin J. Silk', 'Wickham Skinner', 'Howard H. Stevenson', 'Richard S. Tedlow', 'David A. Thomas', 'Peter Tufano', 'Richard E. Walton',
        'Louis T. Wells', 'Michael A. Wheeler', 'Steven C. Wheelwright', 'Michael Y. Yoshino', 'Gerald Zaltman', 'Shoshana Zuboff']

june = ['Rawi E. Abdelal', 'Noubar B. Afeyan', 'David Ager', 'Juan Alcacer', 'Laura Alfaro', 'Elizabeth J. Altman', 'Jose B. Alvarez', 'Teresa M. Amabile', 'Bharat N. Anand', 'Shannon W. Anderson',
        'Lynda M. Applegate', 'Julia B. Austin', 'Jill J. Avery', 'Joseph L. Badaracco', 'Malcolm P. Baker', 'Carliss Y. Baldwin', 'Peter Barrett', 'Julie Battilana', 'Max H. Bazerman', 'Sven Beckert',
        'David E. Bell', 'Edward B. Berk', 'Ethan S. Bernstein', 'John Beshears', 'Amar Bhide', 'Alison Wood Brooks', 'Ryan W. Buell', 'Jeffrey Bussgang', 'Timothy Butler', 'Dennis Campbell',
        'Ramon Casadesus-Masanell', 'Alberto F. Cavallo', 'Frank V. Cespedes', 'Amitabh Chandra', 'Kyle Chauvin', 'Prithwiraj Choudhury', 'Clayton M. Christensen', 'Michael Chu', 'Doug J. Chung',
        'John C. Coates', 'Katherine B. Coffman', 'Lauren H. Cohen', 'Randolph B. Cohen', 'Shawn A. Cole', 'David J. Collis', 'Joshua D. Coval', 'Martha J. Crawford', 'Zoe B. Cullen', 'H. Lawrence Culp',
        'Leemore S. Dafny', 'Srikant M. Datar', 'Thomas J. DeLong', 'Mihir A. Desai', 'Rohit Deshpande', 'Aiyesha Dey', 'Marco Di Maggio', 'Rafael M. Di Tella', 'John D. Dionne', 'Robert J. Dolan',
        'James J. Dowd', 'David F. Drake', 'Benjamin G. Edelman', 'Amy C. Edmondson', 'Mark L. Egan', 'Thomas R. Eisenmann', 'Anita Elberse', 'Caroline M. Elkins', 'Robin J. Ely', 'Willis M. Emmons',
        'Benjamin C. Esty', 'Christine L. Exley', 'Kristin E. Fabbe', 'Chiara Farronato', 'Thomas W. Feeley', 'Kris Johnson Ferreira', 'Sara L. Fleiss', 'C. Fritz Foley', 'Frances X. Frei',
        'Jeremy S. Friedman', 'Walter A. Friedman', 'David G. Fubini', 'Joseph B. Fuller', 'Tristan Gagnon-Bartsch', 'Susanna Gallani', 'Vikram S. Gandhi', 'William W. George', 'Nori Gerardo Lietz',
        'Shikhar Ghosh', 'Valeria Giacomin', 'Stuart C. Gilson', 'Francesca Gino', 'Kathy E. Giusti', 'Joel Goh', 'Lena G. Goldberg', 'Paul A. Gompers', 'John T. Gourville', 'Jerry R. Green', 'Shane Greenstein',
        'Robin Greenwood', 'Daniel P. Gross', 'Allen S. Grossman', 'Boris Groysberg', 'Ranjay Gulati', 'Sunil Gupta', 'Brian J. Hall', 'Richard G. Hamermesh', 'Janice H. Hammond', 'Samuel G. Hanson',
        'Paul M. Healy', 'Jonas Heese', 'Rebecca M. Henderson', 'Regina E. Herzlinger', 'Robert F. Higgins', 'Linda A. Hill', 'Nien-he Hsieh', 'Laura Huang', 'Chester A. Huber', 'Robert S. Huckman',
        'Reshmaan N. Hussam', 'Marco Iansiti', 'Ayelet Israeli', 'Victoria Ivashina', 'Leslie K. John', 'Geoffrey G. Jones', 'Rosabeth M. Kanter', 'Robert S. Kaplan', 'Huseyin S. Karaca', 'Stephen P. Kaufman',
        'Elizabeth A. Keenan', 'Anat Keinan', 'William R. Kerr', 'W. Carl Kester', 'Tarun Khanna', 'Rakesh Khurana', 'John Jong-Hyun Kim', 'William C. Kirby', 'Nancy F. Koehn', 'Elon Kohlberg',
        'Scott Duke Kominers', 'Rembrand M. Koning', 'Mark R. Kramer', 'Joshua Lev Krieger', 'Karim R. Lakhani', 'Rajiv Lal', 'Joseph B. Lassiter', 'Herman B. Leonard', 'Josh Lerner', 'Jennifer M. Logg',
        'Jay W. Lorsch', 'Michael Luca', 'Hong Luo', 'Alan D. MacCormack', 'Alexander J. MacKay', 'John D. Macomber', 'Deepak Malhotra', 'Christopher J. Malloy', 'Daniel Malter', 'Joshua D. Margolis',
        'E. Scott Mayfield', 'Anthony Mayo', 'Emily R. McComb', 'Rory M. McDonald', 'Henry W. McGee', 'Kathleen L. McGinn', 'Paul D. McKinnon', 'Karen Mills', 'Allison H. Mnookin', 'Kevin P. Mohan',
        'Navid Mojir', 'Mihnea C. Moldoveanu', 'Cynthia A. Montgomery', 'Youngme Moon', 'Antonio Moreno', 'David A. Moss', 'Kristin Williams Mugford', 'Gautam Mukunda', 'Ashish Nanda', 'Ramana Nanda',
        'V.G. Narayanan', 'Das Narayandas', 'Tsedal Neeley', 'Donald K. Ngwe', 'Tom Nicholas', 'Nitin Nohria', 'Michael I. Norton', 'Felix Oberholzer-Gee', 'Elie Ofek', 'Lynn S. Paine', 'Krishna G. Palepu',
        'Gerardo Perez Cavazos', 'Leslie A. Perlow', 'Gary P. Pisano', 'Jeffrey T. Polzer', 'Vincent Pons', 'Michael E. Porter', 'Paula A. Price', 'Matthew Rabin', 'Ryan L. Raffaelli', 'Ananth Raman',
        'Karthik Ramanna', 'Lakshmi Ramarajan', 'Jorge Ramirez-Vallejo', 'V. Kasturi Rangan', 'Jeffrey F. Rayport', 'Sophus A. Reinert', 'Forest L. Reinhardt', 'George A. Riedel', 'Meg Rithmire',
        'Jan W. Rivkin', 'Mark N. Roberge', 'Laura Morgan Roberts', 'Steven S. Rogers', 'Dante Roscini', 'Benjamin N. Roth', 'Ethan C. Rouen', 'Richard S. Ruback', 'Raffaella Sadun', 'William A. Sahlman',
        'Tatiana Sandino', 'Shelle M. Santana', 'Laura Phillips Sawyer', 'David S. Scharfstein', 'Leonard A. Schlesinger', 'Amy W. Schulman', 'Kevin A. Schulman', 'Joshua R. Schwartzstein', 'James K. Sebenius',
        'Arthur I Segel', 'Margo Seltzer', 'George Serafeim', 'Kevin W. Sharer', 'Willy C. Shih', 'Andrei Shleifer', 'Anywhere Sikochi', 'Robert Simons', 'Kristin L. Sippl', 'Emil N. Siriwardane',
        'Scott A. Snook', 'Eugene F. Soltes', 'Suraj Srinivasan', 'Erik Stafford', 'Christopher T. Stanton', 'Ariel D. Stern', 'Guhan Subramanian', 'Sandra J. Sucher', 'Adi Sunderam', 'Hirotaka Takeuchi',
        'Thales S. Teixeira', 'Stefan H. Thomke', 'Monique Burns Thompson', 'Michael W. Toffel', 'Gunnar Trumbull', 'Michael L. Tushman', 'Boris Vallee', 'Derek C. M. van Bever', 'Eric J. Van den Steen',
        'Luis M. Viceira', 'Richard H.K. Vietor', 'Eric A. von Hippel', 'Charles C.Y. Wang', 'Andrew Wasynczuk', 'Matthew C. Weinzierl', 'Mitchell B. Weiss', 'John R. Wells', 'Ashley V. Whillans',
        'Robert F. White', 'Emily Williams', 'Christina R. Wing', 'Andy Wu', 'Charles F. Wu', 'Dennis A. Yao', 'David B. Yoffie', 'Sid Yog', 'Royce G. Yudkoff', 'Andy Zelleke', 'Feng Zhu', 'James E. Austin',
        'Christopher A. Bartlett', 'Michael Beer', 'H. Kent Bowen', 'Joseph L. Bower', 'Stephen P. Bradley', 'William J. Bruns', 'James I. Cash', 'Kim B. Clark', 'Dwight B. Crane', 'M. Colyer Crum',
        'John A. Deighton', 'J. Ronald Fox', 'Kenneth A. Froot', 'William E. Fruhan', 'John J. Gabarro', 'Ray A. Goldberg', 'Stephen A. Greyser', 'Myra M. Hart', 'David F. Hawkins', 'Robert H. Hayes',
        'Samuel L. Hayes', 'James L. Heskett', 'Michael C. Jensen', 'John P. Kotter', 'Dorothy A. Leonard', 'Jay O. Light', 'George C. Lodge', 'Paul W. Marshall', 'John H. McArthur', 'F. Warren McFarlan',
        'Robert C. Merton', 'Richard F. Meyer', 'D. Quinn Mills', 'Richard L. Nolan', 'Andre F. Perold', 'Thomas R. Piper', 'William J. Poorvu', 'John W. Pratt', 'John A. Quelch', 'Henry B. Reiling',
        'Alvin E. Roth', 'Malcolm S. Salter', 'W. Earl Sasser', 'Arthur Schleifer', 'Bruce R. Scott', 'Benson P. Shapiro', 'Roy D. Shapiro', 'Alvin J. Silk', 'Wickham Skinner', 'Howard H. Stevenson',
        'Richard S. Tedlow', 'David A. Thomas', 'Peter Tufano', 'Richard E. Walton', 'Louis T. Wells', 'Michael A. Wheeler', 'Steven C. Wheelwright', 'Michael Y. Yoshino', 'Gerald Zaltman', 'Shoshana Zuboff']

left_prelim = []
left = []
arrived = []
arrived_prelim = []

for prof in july:
    if prof in june:
        pass
    else:
        arrived_prelim.append(prof)

for professor in june:
    if professor in july:
        pass
    else:
        left_prelim.append(professor)

for name in left_prelim:
    name_list = name.split()
    if len(name_list[1]) == 1:
        name_list[1] = name_list[1] + '.'
        new_name = ' '.join(name_list)
        left.append(new_name)
    else:
        left.append(name)

for n in arrived_prelim:
    if n in left:
        pass
    else:
        arrived.append(n)

print left
print arrived
