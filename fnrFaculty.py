import requests
from bs4 import BeautifulSoup
import re
import argparse

# !!Remember to input new file name to avoid overwriting last month's data
"""
This script scrapes the HBS Faculty and Research website. 
It produces 3 semicolon-delimited files:
    current faculty
    emeritus faculty
    combined
"""


def remove_semicolon(t):
    if t.find(';') == -1:
        t = t
    else:
        t = re.sub(';', '|', t)
    return t


parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
parser.add_argument('--output-file-current', dest='destination1', required=True)
parser.add_argument('--output-file-emeritus', dest='destination2', required=True)
parser.add_argument('--output-file-all', dest='destination3', required=True)
arguments = parser.parse_args()

outputFileCurrent = arguments.destination1
outputFileEmeritus = arguments.destination2
outputFileAll = arguments.destination3

current_file = open(outputFileCurrent, 'wb')
emeritus_file = open(outputFileEmeritus, 'wb')
all_file = open(outputFileAll, 'wb')

url = 'http://www.hbs.edu/faculty/Pages/browse.aspx'
emeritus_url = 'http://www.hbs.edu/faculty/Pages/browse.aspx?sort=emeriti'

response = requests.get(url)
html = response.content
soup = BeautifulSoup(html, 'lxml')

response2 = requests.get(emeritus_url)
html2 = response2.content
soup2 = BeautifulSoup(html2, 'lxml')

base = 'http://www.hbs.edu'

column_titles = 'Faculty ID;Name;Title;Profile page;Status\n'
current_file.write(column_titles)
all_file.write(column_titles)
emeritus_file.write(column_titles)

current_faculty = soup.findAll(attrs={'class': 'faculty-item'})
for member in current_faculty:
    entry = list()
    name = member.find('div', attrs={'class': 'name'}).text.strip()
    entry.append(name)
    title = member.find('div', attrs={'class': 'title'})
    if title is None:
        title = 'NULL'
    else:
        title = member.find('div', attrs={'class': 'title'}).text
    title = remove_semicolon(title)
    entry.append(title)
    page_link = member.find('a', href=True)
    usable_page_link = page_link['href'].strip()
    link = base + usable_page_link
    entry.append(link)
    faculty_id = re.sub('http://www.hbs.edu/faculty/Pages/profile.aspx\?facId=', '', link)
    entry.insert(0, faculty_id)
    entry.append('current')
    final = ';'.join(entry)
    final = final + '\n'
    final = final.encode('UTF-8')
    current_file.write(final)

    all_file.write(final)

emeritus_faculty = soup2.findAll(attrs={'class': 'faculty-item'})
for member in emeritus_faculty:
    entry = list()
    name = member.find("div", attrs={"class": "name"}).text.strip()
    entry.append(name)
    title = member.find("div", attrs={"class": "title"})
    if title is None:
        title = 'NULL'
    else:
        title = member.find("div", attrs={"class": "title"}).text
    title = remove_semicolon(title)
    entry.append(title)
    page_link = member.find('a', href=True)
    usable_page_link = page_link['href'].strip()
    link = base + usable_page_link
    entry.append(link)
    entry.append('emeritus')
    faculty_id = re.sub('http://www.hbs.edu/faculty/Pages/profile.aspx\?facId=', '', link)
    entry.insert(0, faculty_id)
    final = ';'.join(entry)
    final = final + '\n'
    final = final.encode('UTF-8')
    emeritus_file.write(final)
    all_file.write(final)
