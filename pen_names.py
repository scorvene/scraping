# -*- coding: UTF-8 -*-

import openpyxl as xl

names = open('C:\Users\scorvene\Documents\DP-CMS\pens\pen_names_20181102.xlsx', 'rb')
output_file = open('C:\Users\scorvene\Documents\DP-CMS\pens\pen_names_20181102-2.txt', 'wb')

wb = xl.load_workbook(names)
sheet = wb['Sheet2']

number_of_rows = sheet.max_row

pen_names = dict()
pref_names = dict()

# Create dictionary of faculty_id : pref_name (reverse order)
for n in range(2, number_of_rows + 1):
    faculty_id_cell = 'A' + str(n)
    faculty_id = str(sheet[faculty_id_cell].value)

    pref_last_cell = 'B' + str(n)
    pref_last = sheet[pref_last_cell].value.encode('UTF-8')
    pref_first_cell = 'C' + str(n)
    pref_first = sheet[pref_first_cell].value.encode('UTF-8')

    # add faculty_id : pref_name (reverse order) to pref_names dict
    pref_name_reverse = pref_last + ', ' + pref_first
    pref_names[faculty_id] = pref_name_reverse

    # add faculty_id : pref_name (direct order) to pen_names dict
    pref_name_direct = pref_first + ' ' + pref_last
    pen_names[faculty_id] = [pref_name_direct]

# Add all other pen names to pen_names dict
for n in range(2, number_of_rows + 1):
    faculty_id_cell = 'A' + str(n)
    faculty_id = str(sheet[faculty_id_cell].value)
    pen_last_cell = 'D' + str(n)
    pen_first_cell = 'E' + str(n)
    pen_middle_cell = 'F' + str(n)
    pen_last = sheet[pen_last_cell].value.encode('UTF-8')
    pen_first = sheet[pen_first_cell].value.encode('UTF-8')
    pen_middle = sheet[pen_middle_cell].value

    if pen_middle is None:
        pen_name_inverted = pen_last + ', ' + pen_first
        if pen_name_inverted in pen_names[faculty_id]:
            pass
        elif pen_name_inverted == pref_names[faculty_id]:
            pass
        else:
            pen_names[faculty_id].append(pen_name_inverted)

        pen_name_direct = pen_first + ' ' + pen_last
        if pen_name_direct in pen_names[faculty_id]:
            pass
        else:
            pen_names[faculty_id].append(pen_name_direct)

    else:
        pen_middle = sheet[pen_middle_cell].value.encode('UTF-8')
        pen_name_inverted = pen_last + ', ' + pen_first + ' ' + pen_middle
        if pen_name_inverted in pen_names[faculty_id]:
            pass
        else:
            pen_names[faculty_id].append(pen_name_inverted)
        pen_name_direct = pen_first + ' ' + pen_middle + ' ' + pen_last
        if pen_name_direct in pen_names[faculty_id]:
            pass
        else:
            pen_names[faculty_id].append(pen_name_direct)

# Write column headers to file
output_file.write('Person_ID|Pref_name|Pen_names\n')

# Bring faculty_id, pref_name (reversed) and pen_names together and write to file
for k,v in pen_names.items():
    new_v = ';'.join(v)
    final = str(k) + '|' + pref_names[str(k)] + '|' + new_v
    final = final + '\n'
    output_file.write(final)
