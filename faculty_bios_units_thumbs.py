# -*- coding: UTF-8 -*-

import requests
from bs4 import BeautifulSoup
import re
import openpyxl as xl
import argparse

parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
parser.add_argument('--output-file', dest='out_file', required=True)
parser.add_argument('--fnr-input-file', dest='fnr_file', required=True)
parser.add_argument('--sheet-name', dest='sheet_name', required=True)
arguments = parser.parse_args()

outputFile = arguments.out_file
facultyFile = arguments.fnr_file
worksheet = arguments.sheet_name


def remove_semicolon(t):
    if t.find(';') == -1:
        t = t
    else:
        t = re.sub(';', ' ', t)
    return t


def remove_line_feeds(b):
    if b.find('\n\n') == -1:
        b = b
    else:
        b = re.sub('\n', '', b)
    return b


def remove_print_statement(x):
    if x.find('Print Entire ProfileLess') == -1:
        x = x
    else:
        x = re.sub('Print Entire ProfileLess', '', x)
    return x


output = open(outputFile, 'wb')
f = open(facultyFile, 'rb')
f_and_r_faculty = list()

wb = xl.load_workbook(f)
sheet = wb[worksheet]

number_of_rows = sheet.max_row

for n in range(2, number_of_rows + 1):
    faculty_id_cell = 'A' + str(n)
    faculty_id = str(sheet[faculty_id_cell].value)
    f_and_r_faculty.append(faculty_id)

# print f_and_r_faculty

column_headers = 'hbs-faculty-schema-v3:HBSFacultyId;Name;hbs-faculty-schema-v3:HBSProfilePage;hbs-faculty-schema-v3:HBSFacultyUnit; hbs-faculty-schema-v3:HBSFacultyThumbNail;hbs-faculty-schema-v3:Description;hbs-faculty-schema-v3:DateOfBirth;hbs-faculty-schema-v3:PlaceOfBirth;hbs-faculty-schema-v3:DBpediaResourceURI;hbs-faculty-schema-v3:DBpediaPageURI;hbs-faculty-schema-v3:DBPediaSubtitle\n'

# troublesome_faculty = ['6456', '6551', '10609', '1061497', '6466']

output.write(column_headers)

for item in f_and_r_faculty:
# for item in troublesome_faculty:
    command = 'http://www.hbs.edu/faculty/Pages/profile.aspx\?facId=' + item
    faculty_member = list()
    response = requests.get(command)
    html = response.content
    soup = BeautifulSoup(html, 'lxml')

    faculty_id = item
    faculty_member.append(faculty_id)

    # Get name
    n = soup.find('h1', attrs={'class': 'author'})
    name = n.get_text().strip().encode('UTF-8')
    faculty_member.append(name)
    faculty_member.append(command)
    print 'working on ' + faculty_id + ', ' + name

    # Get unit
    unit_group = soup.find('div', attrs={'class': 'contact-info'})
    unit_child = unit_group.findChildren('a')
    unit = unit_child[0].text.encode('UTF-8')
    if unit == 'Send Email':
        faculty_member.append('NO_UNIT')
    else:
        faculty_member.append(unit)

    # Create thumbnail url
    thumbnail_prefix = 'https://d3vgmmrg377kge.cloudfront.net/photos/facstaff/Ent'
    thumbnail_suffix = '.jpg'
    thumbnail = thumbnail_prefix + str(faculty_id) + thumbnail_suffix

    # Check to be sure that thumbnail actually exists
    thumb_check = requests.get(thumbnail)
    if thumb_check.status_code == 200:
        faculty_member.append(thumbnail)
    else:
        faculty_member.append('NO_THUMBNAIL')

    # Get bio
    bio = soup.find('div', attrs={'class': 'fullbio'})
    children = bio.findChildren('p', recursive=False)

    if len(children) < 1:
        if bio.text.strip() == 'Print Entire ProfileLess':
            faculty_member.append('NO BIO\n')
        elif bio.text is None:
            faculty_member.append('NO BIO\n')
        else:
            brief_bio = bio.text.strip().encode('UTF-8')
            final_brief_bio = remove_semicolon(brief_bio)
            f_brief_bio = remove_line_feeds(final_brief_bio)
            f_brief_bio = f_brief_bio + '\n'
            last_brief_bio = remove_print_statement(f_brief_bio)
            faculty_member.append(last_brief_bio)

    else:
        brief = children[0].text.encode('UTF-8')
        brief = brief.strip()
        final_brief = remove_semicolon(brief)
        final_brief = final_brief + '\n'
        final_final_brief = remove_print_statement(final_brief)
        faculty_member.append(final_final_brief)

    faculty_line_final = ';'.join(faculty_member)
    output.write(faculty_line_final)
