# -*- coding: UTF-8 -*-

# Generates faculty file for 3RDi
# Requires Excel spreadsheet of pen names report from RIS reporting with columns in this order:
# Person Id | Preferred Last Name | Preferred First Name | Pen Name Last | Pen Name First | Pen Name Middle

import openpyxl as xl
import re
import argparse
import requests
from bs4 import BeautifulSoup
from openpyxl import Workbook


def remove_semicolon(t):
    if t.find(';') == -1:
        t = t
    else:
        t = re.sub(';', '|', t)
    return t


def remove_line_feeds(b):
    if b.find('\n\n') == -1:
        b = b
    else:
        b = re.sub('\n', '', b)
    return b


def remove_print_statement(x):
    if x.find('Print Entire ProfileLess') == -1:
        x = x
    else:
        x = re.sub('Print Entire ProfileLess', '', x)
    return x


parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
parser.add_argument('--output-file', dest='output_file', required=True)
parser.add_argument('--output-sheet', dest='output_sheet', required=True)
parser.add_argument('--pen-name-input-file', dest='pen_file', required=True)
parser.add_argument('--sheet-name', dest='sheet_name', required=True)
arguments = parser.parse_args()

penNameFile = arguments.pen_file
worksheet = arguments.sheet_name
outputFile = arguments.output_file
outputSheet = arguments.output_sheet

url = 'http://www.hbs.edu/faculty/Pages/browse.aspx'
emeritus_url = 'http://www.hbs.edu/faculty/Pages/browse.aspx?sort=emeriti'

response = requests.get(url)
html = response.content
soup = BeautifulSoup(html, 'lxml')

response2 = requests.get(emeritus_url)
html2 = response2.content
soup2 = BeautifulSoup(html2, 'lxml')

base = 'http://www.hbs.edu'

fac = dict()

current_faculty = soup.findAll(attrs={'class': 'faculty-item'})
for member in current_faculty:
    page_link = member.find('a', href=True)
    usable_page_link = page_link['href'].strip()
    link = base + usable_page_link
    faculty_id = re.sub('http://www.hbs.edu/faculty/Pages/profile.aspx\?facId=', '', link)

    name = member.find('div', attrs={'class': 'name'}).text.strip().encode('UTF-8')

    fac[faculty_id] = [name]

    title = member.find('div', attrs={'class': 'title'})
    if title is None:
        title = 'NULL'
    else:
        title = member.find('div', attrs={'class': 'title'}).text
    title = title.encode('UTF-8')
    fac[faculty_id].append(title)
    fac[faculty_id].append(link)
    fac[faculty_id].append('current')

emeritus_faculty = soup2.findAll(attrs={'class': 'faculty-item'})
for member in emeritus_faculty:
    page_link = member.find('a', href=True)
    usable_page_link = page_link['href'].strip()
    link = base + usable_page_link
    faculty_id = re.sub('http://www.hbs.edu/faculty/Pages/profile.aspx\?facId=', '', link)

    name = member.find('div', attrs={'class': 'name'}).text.strip().encode('UTF-8')

    fac[faculty_id] = [name]

    title = member.find('div', attrs={'class': 'title'})
    if title is None:
        title = 'NULL'
    else:
        title = member.find('div', attrs={'class': 'title'}).text
    title = title.encode('UTF-8')
    fac[faculty_id].append(title)
    fac[faculty_id].append(link)
    fac[faculty_id].append('emeritus')


names = open(penNameFile, 'rb')

wb = xl.load_workbook(names)
sheet = wb[worksheet]

number_of_rows = sheet.max_row

pen_names = dict()
pref_names = dict()

# Create dictionary of faculty_id : pen names
# Create dictionary of faculty_id : pref_name (reverse order)
for n in range(2, number_of_rows + 1):
    faculty_id_cell = 'A' + str(n)
    faculty_id = str(sheet[faculty_id_cell].value)

    pref_last_cell = 'B' + str(n)
    pref_last = sheet[pref_last_cell].value.encode('UTF-8')
    pref_first_cell = 'C' + str(n)
    pref_first = sheet[pref_first_cell].value.encode('UTF-8')

    # add faculty_id : pref_name to pref_names dict
    pref_name_direct = pref_first + ' ' + pref_last
    pref_names[faculty_id] = pref_name_direct

    # add faculty_id : pref_name (reverse order) to pen_names dict
    pref_name_reverse = pref_last + ', ' + pref_first
    pen_names[faculty_id] = [pref_name_reverse]


# Add all other pen names to pen_names dict
for n in range(2, number_of_rows + 1):
    faculty_id_cell = 'A' + str(n)
    faculty_id = str(sheet[faculty_id_cell].value)
    pen_last_cell = 'D' + str(n)
    pen_first_cell = 'E' + str(n)
    pen_middle_cell = 'F' + str(n)
    pen_last = sheet[pen_last_cell].value.encode('UTF-8')
    pen_first = sheet[pen_first_cell].value.encode('UTF-8')
    pen_middle = sheet[pen_middle_cell].value

    if pen_middle is None:
        pen_name_inverted = pen_last + ', ' + pen_first
        if pen_name_inverted in pen_names[faculty_id]:
            pass
        elif pen_name_inverted == pref_names[faculty_id]:
            pass
        else:
            pen_names[faculty_id].append(pen_name_inverted)

        pen_name_direct = pen_first + ' ' + pen_last
        if pen_name_direct in pen_names[faculty_id]:
            pass
        else:
            pen_names[faculty_id].append(pen_name_direct)

    else:
        pen_middle = sheet[pen_middle_cell].value.encode('UTF-8')
        pen_name_inverted = pen_last + ', ' + pen_first + ' ' + pen_middle
        if pen_name_inverted in pen_names[faculty_id]:
            pass
        else:
            pen_names[faculty_id].append(pen_name_inverted)
        pen_name_direct = pen_first + ' ' + pen_middle + ' ' + pen_last
        if pen_name_direct in pen_names[faculty_id]:
            pass
        else:
            pen_names[faculty_id].append(pen_name_direct)
# print pen_names

counter = 0
thumb_dict = dict()
for item in fac:
    counter += 1
    command = 'http://www.hbs.edu/faculty/Pages/profile.aspx\?facId=' + item
    # faculty_member = list()
    response = requests.get(command)
    html = response.content
    soup = BeautifulSoup(html, 'lxml')

    print 'working on ' + item + ', number ' + str(counter) + ' of ' + str(len(fac))

    # Get unit
    unit_group = soup.find('div', attrs={'class': 'contact-info'})
    unit_child = unit_group.findChildren('a')
    unit = unit_child[0].text.encode('UTF-8')
    if unit == 'Send Email':
        thumb_dict[item] = ['NO_UNIT']
    else:
        thumb_dict[item] = [unit]

    # Create thumbnail url
    thumbnail_prefix = 'https://d3vgmmrg377kge.cloudfront.net/photos/facstaff/Ent'
    thumbnail_suffix = '.jpg'
    thumbnail = thumbnail_prefix + str(item) + thumbnail_suffix

    # Check to be sure that thumbnail actually exists
    thumb_check = requests.get(thumbnail)
    if thumb_check.status_code == 200:
        thumb_dict[item].append(thumbnail)
    else:
        thumb_dict[item].append('NO_THUMBNAIL')

    # Get bio
    bio = soup.find('div', attrs={'class': 'fullbio'})
    children = bio.findChildren('p', recursive=False)

    if len(children) < 1:
        if bio.text.strip() == 'Print Entire ProfileLess':
            thumb_dict[item].append('NO BIO\n')
        elif bio.text is None:
            thumb_dict[item].append('NO BIO\n')
        else:
            brief_bio = bio.text.strip().encode('UTF-8')
            final_brief_bio = remove_semicolon(brief_bio)
            f_brief_bio = remove_line_feeds(final_brief_bio)
            f_brief_bio = f_brief_bio + '\n'
            last_brief_bio = remove_print_statement(f_brief_bio)
            thumb_dict[item].append(last_brief_bio)

    else:
        brief = children[0].text.encode('UTF-8')
        brief = brief.strip()
        final_brief = remove_semicolon(brief)
        final_brief = final_brief + '\n'
        final_final_brief = remove_print_statement(final_brief)
        thumb_dict[item].append(final_final_brief)

# print thumb_dict

wb = Workbook()
wb_name = outputFile
sn = wb.active
sn.title = outputSheet

headers = ['hbs-faculty-schema-v3:HBSFacultyId', 'Name', 'altLabel', 'hbs-faculty-schema-v3:HBSTitle',
           'hbs-faculty-schema-v3:HBSProfilePage', 'hbs-faculty-schema-v3:HBSFacultyUnit',
           'hbs-faculty-schema-v3:DateOfBirth', 'hbs-faculty-schema-v3:PlaceOfBirth',
           'hbs-faculty-schema-v3:DBpediaResourceURI', 'hbs-faculty-schema-v3:DBpediaPageURI',
           'hbs-faculty-schema-v3:DBPediaSubtitle', 'hbs-faculty-schema-v3:HBSFacultyStatus',
           'hbs-faculty-schema-v3:HBSFacultyThumbNail', 'hbs-faculty-schema-v3:Description']

columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']

for entry in range(14):
    header_address = columns[entry] + '1'
    sn[header_address] = headers[entry]

counter = 1
for item in fac:
    counter += 1

    id_address = 'A' + str(counter)
    name_address = 'B' + str(counter)
    pen_address = 'C' + str(counter)
    title_address = 'D' + str(counter)
    profile_address = 'E' + str(counter)
    unit_address = 'F' + str(counter)
    status_address = 'L' + str(counter)
    thumbnail_address = 'M' + str(counter)
    bio_address = 'N' + str(counter)

    sn[id_address] = int(item)
    sn[name_address] = fac[item][0]
    sn[title_address] = fac[item][1]
    sn[profile_address] = fac[item][2]
    sn[status_address] = fac[item][3]
    if item in pen_names:
        pen_list = ';'.join(pen_names[item])
        sn[pen_address] = pen_list
    else:
        name_list = sn[name_address].value.split()
        if len(name_list) == 2:
            p_name = name_list[1] + ', ' + name_list[0]
            sn[pen_address] = p_name
        elif len(name_list) == 3:
            pen_name_list = list()
            p_name = name_list[2] + ', ' + name_list[0] + ' ' + name_list[1]
            pen_name_list.append(p_name)
            p_name_2 = name_list[0] + ' ' + name_list[2]
            pen_name_list.append(p_name_2)
            p_name_3 = name_list[2] + ', ' + name_list[0]
            pen_name_list.append(p_name_3)
            p_names = ';'.join(pen_name_list)
            sn[pen_address] = p_names

    if item in thumb_dict:
        sn[unit_address] = thumb_dict[item][0]
        sn[thumbnail_address] = thumb_dict[item][1]
        sn[bio_address] = thumb_dict[item][2]

wb.save(filename=wb_name)
