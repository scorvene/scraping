# coding=utf-8
import requests
from bs4 import BeautifulSoup
import re
import time

# This script outputs 2 text files, one of faculty personal information and one of works information.
# WORKS


def remove_pipe(t):
    if t.find(' | ') == -1:
        # print 'not found'
        new_t = t
    else:
        # print 'found'
        new_t = re.sub(' \| ', ' ', t)
    return new_t


def get_abstract(x):
    abstract = x.find('div', attrs={'class': 'abstract trim-ellipsis-char'})
    if not abstract:
        return 'NO ABSTRACT'
    else:
        abstract_text = abstract.text.strip().encode('UTF-8')
        if abstract_text.find('\r') == -1 and abstr.find('\n') == -1:
            return abstract_text
        else:
            new_abstract_text = re.sub('\\n', ' ', abstract_text)
            newer_abstract_text = re.sub('\\r', ' ', new_abstract_text)
            return newer_abstract_text


def get_citation(y):
    citation_raw = y.find('div', attrs={'class': 'citation'})
    citation_2 = re.sub('Citation:', '', citation_raw.text)
    citation = re.sub('View Details', '', citation_2)
    citation = citation.encode('UTF-8')
    citation = citation.strip()
    if citation.find('\n') == -1 and citation.find('\r') == -1:
        return citation
    else:
        new_citation = re.sub('\\n', ' ', citation)
        newer_citation = re.sub('\\r', ' ', new_citation)
        return newer_citation


def get_keywords(z):
    keywords = z.findAll('p', attrs={'class': 'keywords'})
    if not keywords:
        return 'NO KEYWORDS'
    else:
        for keyword in keywords:
            kw = keyword.text
            kw = re.sub('Keywords: ', '', kw)
            kw_stripped = re.sub(' \r\n\t\t\t\t  ', '', kw)
            return kw_stripped.encode('UTF-8')


def get_subtype(aa):
    subtype = aa.find('span', attrs={'class': 'subtype'})
    return subtype.text.strip().encode('UTF-8')


def get_ris_id(b):
    ris = b.find('p', attrs={'title'})
    for rr in ris:
        ris_string = rr['href']
        ris_number = re.sub('/faculty/Pages/item\.aspx\?num=', '', ris_string)
        return ris_number


all_items = '&facInfo=pub'
faculty_input = open('hbs_faculty_sample.txt', 'rb')
faculty_works_output = open('fnr_scraper_3_works.txt', 'wb')
faculty_personal_output = open('fnr_scraper_3_personal.txt', 'wb')

for line in faculty_input:
    time.sleep(1)
    faculty_personal_list = list()
    line_list = line.split(';')
    faculty_name = line_list[0]
    faculty_title = line_list[1]
    faculty_page = line_list[2]

    faculty_personal_list.append(faculty_name)
    faculty_personal_list.append(faculty_title)

    url = faculty_page + all_items

    response = requests.get(url)
    html = response.content
    soup = BeautifulSoup(html, 'lxml')

    faculty_units = ['Accounting and Management', 'Business, Government and the International Economy',
                     'Entrepreneurial Management', 'Finance', 'General Management', 'Marketing',
                     'Negotiation, Organizations & Markets', 'Organizational Behavior', 'Strategy',
                     'Technology and Operations Management']
    profile = soup.findAll('div', attrs={'class': 'content-wrap three-column faculty-profile'})
    for item in profile:

        areas_of_interest = soup.findAll('ul', attrs={'class': 'aoi-list'})
        if not areas_of_interest:
            areas = 'NO AOIs'
        else:
            aoi_list = list()
            for area in areas_of_interest:
                aoi = area.findAll('a')
                for a in aoi:
                    aoi_list.append(a.text.strip().encode('UTF-8'))
            areas = ';'.join(aoi_list)
        faculty_personal_list.append(areas)

    fullbio = soup.find(attrs={'class': 'fullbio'})
    if fullbio.text.startswith('\n'):
        full_bio = 'NO BIO'
    else:
        all_bio = fullbio.findAll('p')
        full_bio_lines = list()
        for f in all_bio:
            full_bio_lines.append(f.text.encode('UTF-8'))
        full_bio = ' '.join(full_bio_lines)
    final_bio = remove_pipe(full_bio)
    faculty_personal_list.append(final_bio)

    contact = item.find('div', attrs={'class': 'contact-info'})
    contact_info = contact.findAll('a')

    if len(contact_info) < 2:
        fac_id_string = c_info_item['href']
        if fac_id_string.find('facId') == -1:
            pass
        else:
            fac_id = re.sub('contact\.aspx\?facId=', '', fac_id_string)
            faculty_personal_list.append(fac_id)

        ctct = 'NO FACULTY UNIT'
        faculty_personal_list.append(ctct)

    else:
        contact_list = list()
        for c_info_item in contact_info:
            fac_id_string = c_info_item['href']
            if fac_id_string.find('facId') == -1:
                pass
            else:
                fac_id = re.sub('contact\.aspx\?facId=', '', fac_id_string)
                faculty_personal_list.append(fac_id)

            thing = c_info_item.text.strip().encode('UTF-8')
            if thing == 'Send Email':
                pass
            elif thing in faculty_units:
                contact_list.append(thing)
                cntct = ';'.join(contact_list)
        faculty_personal_list.append(cntct)

    final_personal = '|'.join(faculty_personal_list)
    final_personal = final_personal + '\n'
    faculty_personal_output.write(final_personal)

    content_types = ['Book', 'Published Article', 'Book Component', 'Working Paper',
                     'Course Materials', 'Presentation',  'Unpublished Works']

    for content_type in content_types:
        works = soup.findAll('li', attrs={'data-wcm-edit-label': content_type})
        for work in works:
            faculty_output_list = list()

            ris_id = get_ris_id(work)
            faculty_output_list.append(ris_id)

            faculty_output_list.append(content_type)

            sub = get_subtype(work)
            faculty_output_list.append(sub)

            cite = get_citation(work)
            faculty_output_list.append(cite)

            keys = get_keywords(work)
            faculty_output_list.append(keys)

            abstr = get_abstract(work)
            if abstr.find('\n') == -1:
                final_abstr = abstr
            else:
                final_abstr = re.sub('\n', '', abstr)
            faculty_output_list.append(final_abstr)

            final = '|'.join(faculty_output_list)
            final = final + '\n'
            faculty_works_output.write(final)
