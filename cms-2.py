from lxml import etree
import argparse


parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
parser.add_argument('--output-file', dest='destination', required=True)
arguments = parser.parse_args()

outputFile = arguments.destination
output = open(outputFile, 'wb')

dt = etree.Element('doctype')
root = etree.Element('html', {'xmlns': 'http://www.w3.org/1999/xhtml', 'lang': 'en'})
head = etree.SubElement(root, 'head', {'profile': "http://selenium-ide.openqa.org/profiles/test-case"})
meta = etree.SubElement(head, 'meta', {'http-equiv': "Content-Type", 'content': "text/html; charset=UTF-8"})
link = etree.SubElement(head, 'link', {'rel': "selenium.base", 'href': "https://www.library.hbs.edu/"})
ttl = etree.SubElement(head, 'title')

body = etree.SubElement(root, 'body')
table = etree.SubElement(body, 'table', {"cellpadding": "1", "cellspacing": "1", "border": "1"})
thead = etree.SubElement(table, 'thead')
thead_tr = etree.SubElement(thead, 'tr')
thead_td = etree.SubElement(thead_tr, 'td', {"rowspan": "1", "colspan": "3"})
tbody = etree.SubElement(table, 'tbody')

# tbody_tr = etree.SubElement(tbody, 'tr')
# command_row = etree.SubElement(tbody_tr, 'td')
# target_row = etree.SubElement(tbody_tr, 'td')
# value_row = etree.SubElement(tbody_tr, 'td')

asert = 'assertTitle'
verify = 'verifyText'
click = 'clickAndWait'
wait = 'waitForElementPresent'
more = 'More About This Database'
wait_target = "xpath=//a[@data-link-id='more-link']"
databases_link = 'link=Databases'


xp = ["xpath=(//a[contains(text(),'More About This Database')])[1]",
      "xpath=(//a[contains(text(),'More About This Database')])[163]",
      "xpath=(//a[contains(text(),'More About This Database')])[77]"]
title = ['AAM - Media Intelligence Center', "Women's Wear Daily", 'I/B/E/S']


xxml = "<?xml version='1.0' encoding='UTF-8'?>\n"
doctype_string = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" ' \
                 '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n'


thead_td.text = 'cms'
ttl.text = 'cms'
# command_row.text = 'open'
# target_row.text = '/'
# value_row.text = ''

counter = 0
for n in range(len(xp)):
    counter += 1
    if counter < len(xp)+1:
        tbody_tr = etree.SubElement(tbody, 'tr')
        command_row = etree.SubElement(tbody_tr, 'td')
        target_row = etree.SubElement(tbody_tr, 'td')
        value_row = etree.SubElement(tbody_tr, 'td')
        command_row.text = click
        target_row.text = databases_link
        value_row.text = ''

        tbody_tr = etree.SubElement(tbody, 'tr')
        command_row = etree.SubElement(tbody_tr, 'td')
        target_row = etree.SubElement(tbody_tr, 'td')
        value_row = etree.SubElement(tbody_tr, 'td')
        command_row.text = wait
        target_row.text = wait_target
        value_row.text = ''

        tbody_tr = etree.SubElement(tbody, 'tr')
        command_row = etree.SubElement(tbody_tr, 'td')
        target_row = etree.SubElement(tbody_tr, 'td')
        value_row = etree.SubElement(tbody_tr, 'td')
        command_row.text = verify
        target_row.text = xp[n]
        value_row.text = more

        tbody_tr = etree.SubElement(tbody, 'tr')
        command_row = etree.SubElement(tbody_tr, 'td')
        target_row = etree.SubElement(tbody_tr, 'td')
        value_row = etree.SubElement(tbody_tr, 'td')
        command_row.text = click
        target_row.text = xp[n]
        value_row.text = more

        # tbody_tr = etree.SubElement(tbody, 'tr')
        # command_row = etree.SubElement(tbody_tr, 'td')
        # target_row = etree.SubElement(tbody_tr, 'td')
        # value_row = etree.SubElement(tbody_tr, 'td')
        command_row.text = asert
        target_row.text = title[n]
        value_row.text = ''


et = etree.ElementTree(root)
output.write(xxml)
output.write(doctype_string)
et.write(output, pretty_print=True)
