# -*- coding: UTF-8 -*-

#  Works.  Stopping dev here and moving to faculty_bios_units_thumbs.py to add argparse

import requests
from bs4 import BeautifulSoup
import re
# import argparse


def remove_semicolon(t):
    if t.find(';') == -1:
        t = t
    else:
        t = re.sub(';', ' ', t)
    return t


def remove_line_feeds(b):
    if b.find('\n\n') == -1:
        b = b
    else:
        b = re.sub('\n', '', b)
    return b


def remove_print_statement(x):
    if x.find('Print Entire ProfileLess') == -1:
        x = x
    else:
        x = re.sub('Print Entire ProfileLess', '', x)
    return x


output = open('faculty_bios_3A.txt', 'wb')

troublesome_faculty = ['6456', '6551', '10609', '1061497', '6466']

f_and_r_faculty = ['6628', '1035163', '10637', '178197', '6629', '527239', '6409', '938509', '6410', '6411', '548988',
                   '873098', '176164', '6415', '10639', '566390', '77361', '1119736', '382192', '6420', '12329', '6422',
                   '5718', '17441', '8943', '684820', '320524', '108987', '6873', '10677', '24279', '394129', '126057',
                   '868461', '586364', '327154', '6437', '261321', '643341', '879706', '340063', '6597', '340064',
                   '6660', '1114015', '147411', '879781', '879471', '100041', '220887', '6443', '7196', '6445', '6585',
                   '6447', '340604', '697248', '6448', '768756', '6449', '81020', '417579', '6451', '774513', '6452',
                   '244024', '937841', '7287', '6453', '6454', '774060', '772456', '775022', '1051576', '763972',
                   '773347', '117412', '1059061', '278682', '10647', '6587', '773921', '7945', '102821', '123284',
                   '768512', '774593', '103852', '275677', '447849', '122194', '660784', '6461', '271812', '104417',
                   '731200', '505685', '6463', '6464', '1061497', '6466', '718917', '136446', '772591', '6588', '10650',
                   '263650', '77265', '261323', '6468', '6602', '6470', '333598', '6475', '740159', '12345', '6476',
                   '126103', '6479', '24284', '648943', '139259', '10653', '952941', '6482', '766753', '378483',
                   '1118185', '589473', '24276', '6486', '6487', '126107', '764108', '417571', '337265', '6490',
                   '6491', '6921', '101486', '409749', '6493', '6494', '500905', '876389', '937405', '951435', '240491',
                   '6497', '6498', '271872', '9961', '117991', '6502', '6615', '602417', '601709', '6503', '879788',
                   '92011', '189290', '444656', '10658', '6505', '154993', '282292', '600690', '61618', '6569',
                   '766017', '123725', '116971', '117187', '938857', '10609', '6516', '6589', '1029325', '6518',
                   '98534', '951639', '566431', '6519', '294193', '6520', '6521', '438575', '1061347', '737517',
                   '337264', '6523', '326229', '251462', '13407', '6526', '6527', '775813', '24278', '1134814', '6530',
                   '6627', '736825', '6532', '309413', '257292', '6534', '441533', '496799', '181633', '6535', '6536',
                   '603179', '6538', '48201', '1060423', '594996', '6539', '869446', '189788', '98896', '100370',
                   '1028680', '953217', '6543', '541712', '6544', '10699', '737522', '693127', '13567', '6548',
                   '763959', '564091', '6550', '6551', '15705', '681583', '1050517', '194874', '13527', '879658',
                   '6556', '644441', '773720', '1133317', '164841', '541710', '6558', '10700', '6625', '602452',
                   '735581', '6644', '6610', '333538', '1060330', '6563', '1063486', '522373', '6566', '10762',
                   '386263', '1132519', '140863', '651011', '6584', '738417', '672704', '301858', '6604', '6570',
                   '651677', '104992', '496493', '183463', '13568', '943704', '83234', '956865', '109656', '871877',
                   '447848', '209495', '1051939', '6577', '175449', '589675', '10673', '1047757', '567321', '14938',
                   '1041982', '6413', '6417', '6419', '6421', '6425', '6426', '6428', '6430', '6434', '9917', '6441',
                   '12273', '6446', '12282', '6456', '6457', '6458', '12285', '6467', '6472', '6473', '6474', '6856',
                   '6842', '6484', '6495', '6499', '6501', '7116', '6504', '7005', '6508', '6511', '6513', '6514',
                   '6524', '6528', '6529', '6531', '12310', '6684', '6537', '6594', '6545', '9946', '97593', '6549',
                   '6552', '6553', '6555', '12314', '6560', '6564', '6565', '6567', '12324', '6573', '6575', '6576',
                   '6578', '6579', '6582']

column_headers = 'hbs-faculty-schema-v3:HBSFacultyId;Name;hbs-faculty-schema-v3:HBSProfilePage;hbs-faculty-schema-v3:HBSFacultyUnit; hbs-faculty-schema-v3:HBSFacultyThumbNail;hbs-faculty-schema-v3:Description;hbs-faculty-schema-v3:DateOfBirth;hbs-faculty-schema-v3:PlaceOfBirth;hbs-faculty-schema-v3:DBpediaResourceURI;hbs-faculty-schema-v3:DBpediaPageURI;hbs-faculty-schema-v3:DBPediaSubtitle\n'
output.write(column_headers)

# for item in f_and_r_faculty:
for item in troublesome_faculty:
    command = 'http://www.hbs.edu/faculty/Pages/profile.aspx\?facId=' + item
    faculty_member = list()
    response = requests.get(command)
    html = response.content
    soup = BeautifulSoup(html, 'lxml')

    faculty_id = item
    faculty_member.append(faculty_id)

    # Get name
    n = soup.find('h1', attrs={'class': 'author'})
    name = n.get_text().strip().encode('UTF-8')
    faculty_member.append(name)
    faculty_member.append(command)
    print 'working on ' + faculty_id + ', ' + name

    # Get unit
    unit_group = soup.find('div', attrs={'class': 'contact-info'})
    unit_child = unit_group.findChildren('a')
    unit = unit_child[0].text.encode('UTF-8')
    if unit == 'Send Email':
        faculty_member.append('NO_UNIT')
    else:
        faculty_member.append(unit)

    # Create thumbnail url
    thumbnail_prefix = 'https://d3vgmmrg377kge.cloudfront.net/photos/facstaff/Ent'
    thumbnail_suffix = '.jpg'
    thumbnail = thumbnail_prefix + str(faculty_id) + thumbnail_suffix

    # Check to be sure that thumbnail actually exists
    thumb_check = requests.get(thumbnail)
    if thumb_check.status_code == 200:
        faculty_member.append(thumbnail)
    else:
        faculty_member.append('NO_THUMBNAIL')

    # Get bio
    bio = soup.find('div', attrs={'class': 'fullbio'})
    children = bio.findChildren('p', recursive=False)

    if len(children) < 1:
        if bio.text.strip() == 'Print Entire ProfileLess':
            faculty_member.append('NO BIO\n')
        elif bio.text is None:
            faculty_member.append('NO BIO\n')
        else:
            brief_bio = bio.text.strip().encode('UTF-8')
            final_brief_bio = remove_semicolon(brief_bio)
            f_brief_bio = remove_line_feeds(final_brief_bio)
            f_brief_bio = f_brief_bio + '\n'
            last_brief_bio = remove_print_statement(f_brief_bio)
            faculty_member.append(last_brief_bio)

    else:
        brief = children[0].text.encode('UTF-8')
        brief = brief.strip()
        final_brief = remove_semicolon(brief)
        final_brief = final_brief + '\n'
        final_final_brief = remove_print_statement(final_brief)
        faculty_member.append(final_final_brief)

    faculty_line_final = ';'.join(faculty_member)
    output.write(faculty_line_final)
