import openpyxl as xl
from openpyxl import Workbook

wb = Workbook()
dest_name = 'test.xlsx'

ws1 = wb.active
ws1.title = 'numbers'

counter = 0
for n in range(1, 11):
    counter += 1
    cell_address = 'A' + str(n)
    # print cell_address
    ws1[cell_address] = counter

wb.save(filename=dest_name)