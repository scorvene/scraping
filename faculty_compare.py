import argparse
import openpyxl as xl

"""
Compares 2 Excel files created by fnrFaculty.py
Prints list of who has left and who has arrived
"""

parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
parser.add_argument('--input-file-1', dest='source1', required=True)
parser.add_argument('--input-file-2', dest='source2', required=True)
# parser.add_argument('--output-file', dest='destination', required=True)
# parser.add_argument('--query-column-header', dest='query_column_header', required=True)
arguments = parser.parse_args()

inputFile1 = arguments.source1
inputFile2 = arguments.source2
# outputFile = arguments.destination
# query_header = arguments.query_column_header
# results = outputFile

older_wb = xl.load_workbook(inputFile1)
newer_wb = xl.load_workbook(inputFile2)

older = older_wb['CURRENT']
newer = newer_wb['CURRENT']

older_number_of_rows = older.max_row + 1
newer_number_of_rows = newer.max_row + 1

of_dict = dict()
for n in range(2, older_number_of_rows):
    id_address = 'A' + str(n)
    name_address = 'B' + str(n)
    of_id = older[id_address].value
    of_id = int(of_id)
    of_name = older[name_address].value
    of_dict[of_id] = of_name

nf_dict = dict()
for n in range(2, newer_number_of_rows):
    id_address = 'A' + str(n)
    name_address = 'B' + str(n)
    nf_id = newer[id_address].value
    nf_id = int(nf_id)
    nf_name = newer[name_address].value
    nf_dict[nf_id] = nf_name


arrived = dict()
for k in nf_dict.iterkeys():
    if k in of_dict:
        pass
    else:
        arrived[k] = nf_dict[k]

left = dict()
for key in of_dict.iterkeys():
    if key in nf_dict:
        pass
    else:
        left[key] = of_dict[key]

for a, b in left.iteritems():
    print 'LEFT: ' + b

for i, j in arrived.iteritems():
    print 'ARRIVED: ' + j

# print left
# print arrived
