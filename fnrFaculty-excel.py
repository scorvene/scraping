import requests
from bs4 import BeautifulSoup
import re
import argparse
from openpyxl import Workbook

# !!Remember to input new file name to avoid overwriting last month's data


def remove_semicolon(t):
    if t.find(';') == -1:
        t = t
    else:
        t = re.sub(';', '|', t)
    return t


parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
parser.add_argument('--output-file-all', dest='output_file', required=True)
arguments = parser.parse_args()

outputFile = arguments.output_file

url = 'http://www.hbs.edu/faculty/Pages/browse.aspx'
emeritus_url = 'http://www.hbs.edu/faculty/Pages/browse.aspx?sort=emeriti'

response = requests.get(url)
html = response.content
soup = BeautifulSoup(html, 'lxml')

response2 = requests.get(emeritus_url)
html2 = response2.content
soup2 = BeautifulSoup(html2, 'lxml')

base = 'http://www.hbs.edu'

# Create spreadsheet file and worksheets
wb = Workbook()
wb_name = outputFile
allSheet = wb.active
allSheet.title = 'ALL'
currentSheet = wb.create_sheet('CURRENT')
emeritusSheet = wb.create_sheet('EMERITUS')

# Add column headers to all worksheets
sheets = [allSheet, currentSheet, emeritusSheet]
for s in sheets:
    s['A1'] = 'Faculty ID'
    s['B1'] = 'Name'
    s['C1'] = 'Title'
    s['D1'] = 'Profile Page URL'
    s['E1'] = 'Status'

# Find last row of worksheet
last_row_current = currentSheet.max_row
last_row_all = allSheet.max_row
last_row_emeritus = emeritusSheet.max_row

# Get current faculty
current_faculty = soup.findAll(attrs={'class': 'faculty-item'})
for member in current_faculty:
    # Increment to next row of worksheet
    last_row_current += 1
    last_row_all += 1

    # Get faculty name
    name = member.find('div', attrs={'class': 'name'}).text.strip().encode('UTF-8')
    current_name_cell = 'B' + str(last_row_current)
    currentSheet[current_name_cell] = name

    # Write name to 'current' and 'all' worksheets
    all_name_cell = 'B' + str(last_row_all)
    allSheet[all_name_cell] = name

    # Get faculty title
    title = member.find('div', attrs={'class': 'title'})
    if title is None:
        title = 'NULL'
    else:
        title = member.find('div', attrs={'class': 'title'}).text
    title = remove_semicolon(title)
    prof_title = title.encode('UTF-8')

    # Write title to 'current' and 'all' worksheets
    current_title_cell = 'C' + str(last_row_current)
    all_title_cell = 'C' + str(last_row_all)
    currentSheet[current_title_cell] = prof_title
    allSheet[all_title_cell] = prof_title

    # Get URL of profile page
    page_link = member.find('a', href=True)
    usable_page_link = page_link['href'].strip()
    link = base + usable_page_link

    # Write URL to 'current' and 'all' worksheets
    current_link_cell = 'D' + str(last_row_current)
    currentSheet[current_link_cell] = link
    all_link_cell = 'D' + str(last_row_all)
    allSheet[all_link_cell] = link

    # Extract faculty ID from profile page URL
    faculty_id = re.sub('http://www.hbs.edu/faculty/Pages/profile.aspx\?facId=', '', link)

    # Write URL to 'current' and 'all' worksheets
    current_id_cell = 'A' + str(last_row_current)
    currentSheet[current_id_cell] = faculty_id
    all_id_cell = 'A' + str(last_row_all)
    allSheet[all_id_cell] = faculty_id

    # Write current status to 'current' and 'all' worksheets
    all_status_cell = 'E' + str(last_row_all)
    current_status_cell = 'E' + str(last_row_all)
    allSheet[all_status_cell] = 'current'
    currentSheet[current_status_cell] = 'current'


# Get emeritus faculty
emeritus_faculty = soup2.findAll(attrs={'class': 'faculty-item'})
for member in emeritus_faculty:
    # Increment to next row of worksheets
    last_row_emeritus += 1
    last_row_all += 1

    # Get faculty name
    name = member.find('div', attrs={'class': 'name'}).text.strip().encode('UTF-8')

    # Write name to 'emeritus' and 'all' worksheets
    emeritus_name_cell = 'B' + str(last_row_emeritus)
    emeritusSheet[emeritus_name_cell] = name
    all_name_cell = 'B' + str(last_row_all)
    allSheet[all_name_cell] = name

    # Get faculty title
    title = member.find('div', attrs={'class': 'title'})
    if title is None:
        title = 'NULL'
    else:
        title = member.find('div', attrs={'class': 'title'}).text
    title = remove_semicolon(title)
    prof_title = title.encode('UTF-8')

    # Write title to 'emeritus' and 'all' worksheets
    emeritus_title_cell = 'C' + str(last_row_emeritus)
    all_title_cell = 'C' + str(last_row_all)
    emeritusSheet[emeritus_title_cell] = prof_title
    allSheet[all_title_cell] = prof_title

    # Get URL of profile page
    page_link = member.find('a', href=True)
    usable_page_link = page_link['href'].strip()
    link = base + usable_page_link

    # Write URL to 'emeritus' and 'all' worksheets
    emeritus_link_cell = 'D' + str(last_row_emeritus)
    emeritusSheet[emeritus_link_cell] = link
    all_link_cell = 'D' + str(last_row_all)
    allSheet[all_link_cell] = link

    # Extract faculty ID from profile page URL
    faculty_id = re.sub('http://www.hbs.edu/faculty/Pages/profile.aspx\?facId=', '', link)

    # Write URL to 'current' and 'all' worksheets
    emeritus_id_cell = 'A' + str(last_row_emeritus)
    emeritusSheet[emeritus_id_cell] = faculty_id
    all_id_cell = 'A' + str(last_row_all)
    allSheet[all_id_cell] = faculty_id

    # Write emeritus status to 'emeritus' and 'all' worksheets
    all_status_cell = 'E' + str(last_row_all)
    emeritus_status_cell = 'E' + str(last_row_emeritus)
    allSheet[all_status_cell] = 'emeritus'
    emeritusSheet[emeritus_status_cell] = 'emeritus'

wb.save(filename=wb_name)
