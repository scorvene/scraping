from lxml import etree
import argparse

"""
This script was written to generate an html for Selenium testing of links and content on the Baker Library website.
This is a section of what will be the completed script.  This part tests the generation of the html that Selenium
will use.  The script that scrapes the website for links and titles is in the Vagrant virtual box.
"""

parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
parser.add_argument('--output-file', dest='destination', required=True)
arguments = parser.parse_args()

outputFile = arguments.destination
output = open(outputFile, 'wb')

root = etree.Element('html', {'xmlns': 'http://www.w3.org/1999/xhtml', 'lang': 'en'})
head = etree.SubElement(root, 'head', {'profile': 'http://selenium-ide.openqa.org/profiles/test-case'})
meta = etree.SubElement(head, 'meta', {'http-equiv': 'Content-Type', 'content': 'text/html; charset=UTF-8'})
link = etree.SubElement(head, 'link', {'rel': 'selenium.base', 'href': 'https://www.library.hbs.edu/'})
ttl = etree.SubElement(head, 'title')

body = etree.SubElement(root, 'body')
table = etree.SubElement(body, 'table', {'cellpadding': '1', 'cellspacing': '1', 'border': '1'})
thead = etree.SubElement(table, 'thead')
thead_tr = etree.SubElement(thead, 'tr')
thead_td = etree.SubElement(thead_tr, 'td', {'rowspan': '1', 'colspan': '3'})
tbody = etree.SubElement(table, 'tbody')

asert = 'assertTitle'
verify = 'verifyText'
click = 'clickAndWait'
wait = 'waitForElementPresent'
more = 'More About This Database'
wait_target = "xpath=//a[@data-link-id='more-link']"
databases_link = 'link=Databases'
databases_value = 'Databases'
home_page_title = 'Baker Library | Bloomberg Center | Harvard Business School'

xp = ["xpath=(//a[contains(text(),'More About This Database')])[1]",
      "xpath=(//a[contains(text(),'More About This Database')])[163]",
      "xpath=(//a[contains(text(),'More About This Database')])[77]"]
title = ['AAM - Media Intelligence Center | Baker Library | Harvard Business School',
         "Women's Wear Daily | Baker Library | Harvard Business School",
         'I/B/E/S | Baker Library | Harvard Business School']


xxml = "<?xml version='1.0' encoding='UTF-8'?>\n"
doctype_string = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" ' \
                 '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n'


def table_rows(command, target, value):
    tbody_tr = etree.SubElement(tbody, 'tr')
    command_row = etree.SubElement(tbody_tr, 'td')
    command_row.text = command
    target_row = etree.SubElement(tbody_tr, 'td')
    target_row.text = target
    value_row = etree.SubElement(tbody_tr, 'td')
    value_row.text = value

table_rows('open', '/', '')
table_rows(asert, home_page_title, '')
table_rows(verify, databases_link, databases_value)

thead_td.text = 'cms'
ttl.text = 'cms'

counter = 0
for n in range(len(xp)):
    counter += 1
    if counter < len(xp)+1:
        table_rows(click, databases_link, '')
        table_rows(wait, wait_target, '')
        table_rows(verify, xp[n], more)
        table_rows(click, xp[n], more)
        table_rows(asert, title[n], '')


et = etree.ElementTree(root)
output.write(xxml)
output.write(doctype_string)
et.write(output, pretty_print=True)
