names = ['Edward B. Berk', 'Teresa M. Amabile', 'Bharat N. Anand', 'Lynda M. Applegate', 'James E. Austin',
         'Joseph L. Badaracco', 'Carliss Y. Baldwin', 'Christopher A. Bartlett', 'Max H. Bazerman', 'Michael Beer',
         'David E. Bell', 'H. Kent Bowen', 'Joseph L. Bower', 'Stephen P. Bradley', 'William J. Bruns', 'James I. Cash',
         'Clayton M. Christensen', 'Dwight B. Crane', 'Srikant M. Datar', 'Thomas J. DeLong', 'John A. Deighton',
         'Rohit Deshpande', 'Rafael M. Di Tella', 'Robert J. Dolan', 'Amy C. Edmondson', 'Thomas R. Eisenmann',
         'Willis M. Emmons', 'Benjamin C. Esty', 'Kenneth A. Froot', 'William E. Fruhan', 'John J. Gabarro',
         'Stuart C. Gilson', 'Paul A. Gompers', 'John T. Gourville', 'Jerry R. Green', 'Stephen A. Greyser',
         'Brian J. Hall', 'Janice H. Hammond', 'Myra M. Hart', 'David F. Hawkins', 'Robert H. Hayes', 'Paul M. Healy',
         'Regina E. Herzlinger', 'Linda A. Hill', 'Marco Iansiti', 'Michael C. Jensen', 'Rosabeth M. Kanter',
         'Robert S. Kaplan', 'W. Carl Kester', 'Tarun Khanna', 'Nancy F. Koehn', 'Elon Kohlberg', 'John P. Kotter',
         'Rajiv Lal', 'Joseph B. Lassiter', 'Dorothy A. Leonard', 'Jay O. Light', 'Jay W. Lorsch', 'Alan D. MacCormack',
         'Paul W. Marshall', 'E. Scott Mayfield', 'F. Warren McFarlan', 'Robert C. Merton', 'Richard F. Meyer',
         'D. Quinn Mills', 'Cynthia A. Montgomery', 'David A. Moss', 'Ashish Nanda', 'V.G. Narayanan', 'Das Narayandas',
         'Nitin Nohria', 'Richard L. Nolan', 'Lynn S. Paine', 'Krishna G. Palepu', 'Andre F. Perold', 'Thomas R. Piper',
         'Gary P. Pisano', 'William J. Poorvu', 'Michael E. Porter', 'Ananth Raman', 'V. Kasturi Rangan',
         'Jeffrey F. Rayport', 'Henry B. Reiling', 'Forest L. Reinhardt', 'Jan W. Rivkin', 'Richard S. Ruback',
         'William A. Sahlman', 'Malcolm S. Salter', 'Leonard A. Schlesinger', 'Bruce R. Scott', 'James K. Sebenius',
         'Arthur I Segel', 'Benson P. Shapiro', 'Roy D. Shapiro', 'Alvin J. Silk', 'Robert Simons', 'Debora L. Spar',
         'Howard H. Stevenson', 'Hirotaka Takeuchi', 'Richard S. Tedlow', 'David A. Thomas', 'Stefan H. Thomke',
         'Peter Tufano', 'Kathleen L. McGinn', 'Richard H.K. Vietor', 'Louis T. Wells', 'Michael A. Wheeler',
         'Steven C. Wheelwright', 'David B. Yoffie', 'Michael Y. Yoshino', 'Gerald Zaltman', 'Shoshana Zuboff',
         'Michael L. Tushman', 'Mihir A. Desai', 'Frances X. Frei', 'Allen S. Grossman', 'Youngme Moon',
         'Alvin E. Roth', 'Randolph B. Cohen', 'Richard G. Hamermesh', 'Luis M. Viceira', 'Sandra J. Sucher',
         'Gary W. Loveman', 'Erik Stafford', 'Jeffrey T. Polzer', 'Rawi E. Abdelal', 'Laura Alfaro',
         'Guhan Subramanian', 'David J. Collis', 'John A. Quelch', 'James L. Heskett', 'Samuel L. Hayes',
         'Timothy Butler', 'Rakesh Khurana', 'John H. McArthur', 'George C. Lodge', 'Antonio Davila',
         'Robin J. Ely', 'Walter A. Friedman', 'John Beshears', 'Kim B. Clark', 'W. Earl Sasser', 'Josh Lerner',
         'Mihnea C. Moldoveanu', 'David Ager', 'Malcolm P. Baker', 'C. Fritz Foley', 'Boris Groysberg',
         'Robert S. Huckman', 'Joshua D. Margolis', 'Andy Zelleke', 'Dennis Campbell', 'Tatiana Sandino',
         'Suraj Srinivasan', 'Monique Burns Thompson', 'M. Colyer Crum', 'J. Ronald Fox', 'Ray A. Goldberg',
         'John W. Pratt', 'Wickham Skinner', 'Richard E. Walton', 'Sven Beckert', 'Rebecca M. Henderson', 'Elie Ofek',
         'Andrei Shleifer', 'David S. Scharfstein', 'John R. Wells', 'Feng Zhu', 'George Serafeim',
         'Ethan S. Bernstein', 'Geoffrey G. Jones', 'Leslie A. Perlow', 'Ramon Casadesus-Masanell', 'Nien-he Hsieh',
         'George A. Riedel', 'Henry W. McGee', 'Ranjay Gulati', 'Peter Barrett', 'James J. Dowd', 'Robert F. White',
         'John D. Macomber', 'Arthur Schleifer', 'Kristin Williams Mugford', 'Steven S. Rogers', 'H. Lawrence Culp',
         'Dante Roscini', 'John Jong-Hyun Kim', 'David G. Fubini', 'Vikram S. Gandhi', 'Kathy E. Giusti',
         'Andrew Wasynczuk', 'Jeffrey Bussgang', 'Christina R. Wing', 'Allison H. Mnookin', 'Kevin P. Mohan',
         'Trevor Fetter', 'Stig Leschly', 'Shikhar Ghosh', 'Joseph B. Fuller', 'Karen Mills', 'Frank V. Cespedes',
         'Robert F. Higgins', 'Stephen P. Kaufman', 'Robin Greenwood', 'Chester A. Huber', 'Gunnar Trumbull',
         'Joshua D. Coval', 'Anthony Mayo', 'Scott A. Snook', 'Sid Yog', 'Jill J. Avery', 'Juan Alcacer',
         'Jorge Ramirez-Vallejo', 'Mitchell B. Weiss', 'Deepak Malhotra', 'Willy C. Shih', 'Dennis A. Yao',
         'Leemore S. Dafny', 'Karim R. Lakhani', 'Anita Elberse', 'Felix Oberholzer-Gee', 'Ryan L. Raffaelli',
         'Michael Chu', 'Sunil Gupta', 'Yael Grushka-Cockayne', 'Francesca Gino', 'Herman B. Leonard',
         'William W. George', 'Sara L. Fleiss', 'Emily R. McComb', 'Ramana Nanda', 'Eric J. Van den Steen',
         'Matthew Rabin', 'Ryan W. Buell', 'Michael I. Norton', 'Prithwiraj Choudhury', 'Adi Sunderam',
         'Samuel G. Hanson', 'Tom Nicholas', 'William R. Kerr', 'Lauren H. Cohen', 'Shawn A. Cole', 'Aiyesha Dey',
         'Victoria Ivashina', 'Julie Battilana', 'Michael W. Toffel', 'Alberto F. Cavallo', 'William C. Kirby',
         'Anat Keinan', 'Benjamin G. Edelman', 'Tsedal Neeley', 'Karthik Ramanna', 'Christopher J. Malloy',
         'Charles F. Wu', 'Nori Gerardo Lietz', 'Matthew C. Weinzierl', 'Lakshmi Ramarajan', 'Scott Duke Kominers',
         'Lena G. Goldberg', 'Thales S. Teixeira', 'Jose B. Alvarez', 'Eugene F. Soltes', 'Raffaella Sadun',
         'Eva Ascarza', 'Joshua R. Schwartzstein', 'Kate Barasz', 'Frank Nagle', 'Ting Zhang', 'Kyle Chauvin',
         'Leslie K. John', 'Royce G. Yudkoff', 'Meg Rithmire', 'Rory M. McDonald', 'Hong Luo', 'Michael Luca',
         'Christopher T. Stanton', 'Sophus A. Reinert', 'Doug J. Chung', 'Kristin L. Sippl', 'Laura Huang',
         'Chinmay Tumbe', 'Charles C.Y. Wang', 'Hise Orenthial Gibson', 'Derek C. M. van Bever', 'Kevin W. Sharer',
         'Alison Wood Brooks', 'Laura Phillips Sawyer', 'Marco Di Maggio', 'Shane Greenstein', 'Joel Goh',
         'Ariel D. Stern', 'Vincent Pons', 'Donald K. Ngwe', 'Shelle M. Santana', 'Boris Vallee', 'Jonas Heese',
         'Amy W. Schulman', 'Thomas W. Feeley', 'Elizabeth A. Keenan', 'Paul D. McKinnon', 'Ayelet Israeli',
         'Tristan Gagnon-Bartsch', 'John D. Dionne', 'Kristin E. Fabbe', 'Daniel P. Gross', 'Kris Johnson Ferreira',
         'Emil N. Siriwardane', 'Jeremy S. Friedman', 'Christine L. Exley', 'Mark L. Egan', 'Susanna Gallani',
         'Chiara Farronato', 'Gerardo Perez Cavazos', 'Amitabh Chandra', 'Mark N. Roberge', 'Andy Wu',
         'Julia B. Austin', 'Rembrand M. Koning', 'Zoe B. Cullen', 'Anywhere Sikochi', 'Katherine B. Coffman',
         'Martha J. Crawford', 'Alexander J. MacKay', 'Mark R. Kramer', 'Caroline M. Elkins', 'Tomomichi Amano',
         'Navid Mojir', 'Ashley V. Whillans', 'Joshua Lev Krieger', 'Kyle R. Myers', 'Reshmaan N. Hussam',
         'Ethan C. Rouen', 'Emily Williams', 'Benjamin N. Roth', 'Antonio Moreno', 'Noubar B. Afeyan',
         'Julian J. Zlatev', 'Letian Zhang', 'Sudev J Sheth', 'Giovanni Favero', 'Michael H. Yeomans',
         'Mattias E. Fibiger', 'Marco E. Tabellini', 'Natalia Rigol', 'Trung Nguyen', 'Daniel W. Green',
         'Jorge Tamayo', 'Daniel Corsten', 'Srikanth Jagabathula', 'John F. Batter', 'Srinivas Reddy',
         'Brian L Trelstad', 'Mario Small', 'Caspar David Peter']

for name in names:
    name_list = name.split()
    if len(name_list) == 2:
        pen_name = name_list[1] + ', ' + name_list[0]
        print pen_name
    elif len(name_list) == 3:
        pen_name = name_list[2] + ', ' + name_list[0] + ' ' + name_list[1]
        print pen_name
    elif len(name_list) == 4:
        pen_name = name_list[2] + ' ' + name_list[3] + ', ' + name_list[0] + ' ' + name_list[1]
        print pen_name
    elif len(name_list) == 5 and len(name_list[2]) > 2:
        pen_name = name_list[2] + ' ' + name_list[3] + ' ' + name_list[4] + ', ' + name_list[0] + ' ' + name_list[1]
        print pen_name
    else:
        pen_name = name_list[3] + ' ' + name_list[4] + ', ' + name_list[0] + ' ' + name_list[1] + ' ' + name_list[2]
        print pen_name
