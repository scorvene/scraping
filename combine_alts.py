# -*- coding: UTF-8 -*-

import openpyxl as xl

names = open('C:/Users/scorvene/Desktop/pens/HBS faculty 20180904 for 3RDi.xlsx', 'rb')
output_file = open('C:/Users/scorvene/Desktop/pens/alts_combined.txt', 'wb')

wb = xl.load_workbook(names)
sheet = wb['Sheet1']

number_of_rows = sheet.max_row


for n in range(2, number_of_rows):
    alt_list = list()
    faculty_id_cell = 'C' + str(n)
    faculty_id = str(sheet[faculty_id_cell].value)
    faculty_id = faculty_id + '|'
    alt_list.append(faculty_id)

    alt_1_cell = 'B' + str(n)
    alt_1 = sheet[alt_1_cell].value.encode('UTF-8')
    alt_list.append(alt_1)

    alt_2_cell = 'D' + str(n)
    alt_2 = sheet[alt_2_cell].value
    if alt_2 is not None:
        alt_2 = sheet[alt_2_cell].value.encode('UTF-8')
        alt_list.append(alt_2)
    else:
        pass

    final = ';'.join(alt_list)
    final = final + '\n'
    output_file.write(final)
