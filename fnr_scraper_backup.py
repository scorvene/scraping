import requests
from bs4 import BeautifulSoup
import re
import time


def get_abstract(x):
    abstract = x.find('div', attrs={'class': 'abstract trim-ellipsis-char'})
    if not abstract:
        return 'NO ABSTRACT'
    else:
        abstract_text = abstract.text
        # return abstract_text
        if abstract_text.find('\r') == -1 and abstr.find('\n') == -1:
            return abstract.text.strip().encode('UTF-8')
        else:
            new_abstract_text = re.sub('\\n', ' ', abstract_text)
            newer_abstract_text = re.sub('\\r', ' ', new_abstract_text)
            return newer_abstract_text.strip().encode('UTF-8')


def get_citation(y):
    citation_raw = y.find('div', attrs={'class': 'citation'})
    citation_2 = re.sub('Citation:', '', citation_raw.text)
    citation = re.sub('View Details', '', citation_2)
    citation = citation.encode('UTF-8')
    return citation.strip()


def get_keywords(z):
    keywords = z.findAll('p', attrs={'class': 'keywords'})
    if not keywords:
        return 'NO KEYWORDS'
    else:
        for keyword in keywords:
            kw = keyword.text
            kw = re.sub('Keywords: ', '', kw)
            kw_stripped = re.sub(' \r\n\t\t\t\t  ', '', kw)
            return kw_stripped.encode('UTF-8')


def get_subtype(aa):
    subtype = aa.find('span', attrs={'class': 'subtype'})
    return subtype.text.strip().encode('UTF-8')


def get_ris_id(b):
    ris = b.find('p', attrs={'title'})
    for thing in ris:
        ris_string = thing['href']
        ris_number = re.sub('/faculty/Pages/item\.aspx\?num=', '', ris_string)
        return ris_number

all_items = '&facInfo=pub'
faculty_input = open('hbs_faculty_sample.txt', 'rb')
faculty_output = open('fnr_scraper_2_test.txt', 'wb')
for line in faculty_input:
    time.sleep(1)
    faculty_output_list = list()
    line_list = line.split(';')
    faculty_name = line_list[0]
    faculty_title = line_list[1]
    faculty_page = line_list[2]

    faculty_output_list.append(faculty_name)
    faculty_output_list.append(faculty_title)
    # print faculty_name
    # print faculty_title
    url = faculty_page + all_items

# url = 'http://www.hbs.edu/faculty/Pages/profile.aspx?facId=6628&facInfo=pub'
# url = 'http://www.hbs.edu/faculty/Pages/profile.aspx?facId=6532&facInfo=pub'
# url = 'http://www.hbs.edu/faculty/Pages/profile.aspx?facId=6409&facInfo=pub'
# url = 'http://www.hbs.edu/faculty/Pages/profile.aspx?facId=55039&facInfo=pub'

    response = requests.get(url)
    html = response.content
    soup = BeautifulSoup(html, 'lxml')

    profile = soup.findAll('div', attrs={'class': 'content-wrap three-column faculty-profile'})
    for item in profile:
        contact = item.find('div', attrs={'class': 'contact-info'})
        cs = contact.findAll('a')
        for c in cs:
            if c.text.find('Send') == -1:
                # print c.text
                faculty_output_list.append(c.text.encode('UTF-8'))
            else:
                fac_id_string = c['href']
                fac_id = re.sub('contact\.aspx\?facId=', '', fac_id_string)
                # print fac_id
                faculty_output_list.append(fac_id)

        areas_of_interest = soup.findAll('ul', attrs={'class': 'aoi-list'})
        if not areas_of_interest:
            # print 'no aoi'
            faculty_output_list.append('NO AOIs')
        else:
            aoi_list = list()
            for area in areas_of_interest:
                aoi = area.findAll('a')
                for a in aoi:
                    aoi_list.append(a.text.strip().encode('UTF-8'))
            areas = ';'.join(aoi_list)
            # print areas
            faculty_output_list.append(areas)

    fullbio = soup.find(attrs={'class': 'fullbio'})
    if fullbio.text.startswith('\n'):
        # print 'NO BIO'
        faculty_output_list.append('NO BIO')
    else:
        all_bio = fullbio.findAll('p')
        full_bio_lines = list()
        for f in all_bio:
            full_bio_lines.append(f.text.encode('UTF-8'))
        full_bio = ' '.join(full_bio_lines)
        faculty_output_list.append(full_bio)
        # print full_bio

    content_types = ['Book', 'Published Article', 'Book Component', 'Working Paper',
                     'Course Materials', 'Presentation',  'Unpublished Works']

    for content_type in content_types:
        works = soup.findAll('li', attrs={'data-wcm-edit-label': content_type})
        for work in works:
            # print content_type
            faculty_output_list.append(content_type)

            sub = get_subtype(work)
            # print sub
            faculty_output_list.append(sub)

            cite = get_citation(work)
            # print cite
            faculty_output_list.append(cite)

            keys = get_keywords(work)
            # print keys
            faculty_output_list.append(keys)

            ris_id = get_ris_id(work)
            # print ris_id
            faculty_output_list.append(ris_id)

            abstr = get_abstract(work)
            # print abstr
            faculty_output_list.append(abstr)

            # print '\n'
    # print faculty_output_list
    final = '|'.join(faculty_output_list)
    final = final + '\n'
    print final
    faculty_output.write(final)
