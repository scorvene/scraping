file name:  cms-2.py
author:     scorvene
function:   Generates an HTML file to be used in Selenium testing of CMS database page
notes:      Part of development process
            Never used
            Testing process abandoned; superseded by mabl

file name:  cms-3.py
author:     scorvene
function:   This script was written to generate an html for Selenium testing of links and content on the Baker Library
            website.  This is a section of what will be the completed script.  This part tests the generation of the
            html that Selenium will use.  The script that scrapes the website for links and titles is in the Vagrant
            virtual box.
notes:      Part of development process
            Never used
            Testing process abandoned; superseded by mabl

file name:  faculty_bio.py
author:     scorvene
function:   Scrapes the faculty pages listed for first paragraph of full bio
notes:      Output needs human interpretation.  Some first paragraphs consist of only a title.

file name:  fnrFaculty.py
author:     scorvene
function:   Scrapes the HBS Faculty & Research website to generate a list of current and emeritus faculty
            Generates 3 text files: current faculty, emeritus faculty, all faculty
notes:      Run monthly on the first of the month

file name:  fnr_scraper.py
author:     scorvene
function:   Scrapes F&R for personal and works information
            Outputs a file with one line per faculty member containing all personal and work info
notes:      Output unwieldy.  Never used.

file name:  fnr_scraper_2.py
author:     scorvene
function:   Scrapes F&R for personal and works information
            Outputs a file with one line for each work. Each line also contains faculty personal info.
notes:

file name:  fnr_scraper_3.py
author:     scorvene
function:   Scrapes F&R for personal and works information
            Outputs 2 files, one with faculty personal info, one with works info
notes:

file name:  fnr_scraper_backup.py
author:     scorvene
function:   backup copy of fnr_scraper.py
notes:

file name:  protwords.py
author:     scorvene
function:   Scrapes the faculty list page of F&R to generate a list of faculty names to be added to search strategy
            as protected words
notes:

file name:  termstore.py
author:     scorvene
function:   An attempt to scrape termstore to get a list of what is in the repository
notes:      Not working.  I have been unable to get the session to log in and always get a 403 FORBIDDEN response

file name:  test.py
author:     scorvene
function:   Sandbox script to work out bits of code.  Currently empty.
notes:


