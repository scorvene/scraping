import requests
from bs4 import BeautifulSoup
import re
import argparse

# this works to get level 1 terms correctly

parser = argparse.ArgumentParser(description="Specify input and output files on the command-line")
parser.add_argument('--taxonomy-level-1', dest='taxo', required=True)
parser.add_argument('--output-file', dest='level_1_frequency', required=True)
arguments = parser.parse_args()

level_1 = arguments.taxo
out = arguments.level_1_frequency

search_terms = open(level_1, 'rb')
f = open(out, 'wb')

url = 'https://www.hbs.edu/faculty/Pages/search.aspx?searchtype=&tp='

# Create list of level 1 terms to be searched
terms = list()
for t in search_terms:
    t = t.strip().encode('UTF-8')
    terms.append(t)

# Create list of terms which have already been searched
# After each term is searched it will be added to this list to prevent it being searched again
already_searched = list()


for term in terms:
    print 'working on ' + term
    search = url + term

    response = requests.get(search)
    html = response.content
    soup = BeautifulSoup(html, 'lxml')

    list_items = soup.findAll('li')
    for item in list_items:
        if item.text is not None:
            i = item.text.encode('UTF-8')

            if i.find("(") != -1:
                item_list = i.split("(")
                if item_list[0].strip() == term:
                    if item_list[0].strip not in already_searched:
                        term = item_list[0].strip()
                        already_searched.append(term)
                        # print already_searched
                        # new_i = re.sub('\t', '', i)
                        # final_i = re.sub('\s\s', '', new_i)
                        # final_i_list = final_i.split("(")
                        # term = final_i_list[0].strip()
                        # print 'working on ' + term
                        amount = re.sub("\)", '', item_list[1])
                        data = term + ';' + amount
                        data = data + '\n'
                        # print data
                        f.write(data)
                    else:
                        pass
print already_searched

