from bs4 import BeautifulSoup

html_doc = """
<html>
    <head>
        <title>The Dormouse's HTML story</title>
    </head>
    <body>
        <p class="title">
            <b>The Dormouse's story</b></p>

<p class="story">Once upon a time there were three little sisters; and their names were
<a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
<a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
<a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>; and they lived at the bottom of a well.
Their brother <a href="http://example.com/sherman" class="brother" id="link4">Sherman</a> lived in the next well
over.</p> <p class="story">...</p>
"""

soup = BeautifulSoup(html_doc, 'html.parser')
print 'soup.prettify: '
print soup.prettify()
print "title element: " + str(soup.title)
print 'soup.title.name: ' + soup.title.name
print 'soup.title.string: ' + soup.title.string
print 'soup.body.parent.name: ' + soup.body.parent.name
print 'soup.p[class]:'
print soup.p['class']
print 'soup.findAll(a):'
print soup.findAll('a')
print
for link in soup.findAll('a'):
    print link.get('href')
print
print soup.findAll('a', {'class': 'brother'})
print
for link in soup.findAll('a', {'class': 'sister'}):
    print link.get('href')
# print soup.find(id='link4')

print soup.get_text()

