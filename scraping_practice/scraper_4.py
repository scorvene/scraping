import requests
from bs4 import BeautifulSoup
import csv

"""This scraper works."""

url = 'https://report.boonecountymo.org/mrcjava/servlet/SH01_MP.I00290s'
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html)
table = soup.find('tbody', attrs={'class', 'stripe'})

row_list = list()
for row in table.findAll('tr'):
    cell_list = list()
    for cell in row.findAll('td'):
        text = cell.text.encode('utf-8')
        cell_list.append(text)
    row_list.append(cell_list)

# print row_list
outfile = open('inmates.csv', 'wb')
writer = csv.writer(outfile)
writer.writerow(["Last", "First", "Middle", "Gender", "Race", "Age", "City", "State"])
writer.writerows(row_list)

