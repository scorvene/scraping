National Flag: ;
Area: ;1,580 square kilometres
Population: ;26,711
Iso: ;AX
Country: ;Aland Islands
Capital: ;Mariehamn
Continent: ;EU
Tld: ;.ax
Currency Code: ;EUR
Currency Name: ;Euro
Phone: ;+358-18
Postal Code Format: ;#####
Postal Code Regex: ;^(?:FI)*(\d{5})$
Languages: ;sv-AX
Neighbours: ; 
