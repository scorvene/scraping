import requests
from bs4 import BeautifulSoup

"""
This script scrapes the page for the Aland Islands from webscraping.com
as a semicolon-delimited text fiLe
"""

url = 'http://example.webscraping.com/view/Aland-Islands-2'
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html)
table = soup.find('table')

outfile = open('Aland-2.txt', 'wb')

for row in table.findAll('tr'):
    cell_list = list()
    for cell in row.findAll('label'):
        label_text = cell.text
        cell_list.append(label_text)
    for cell in row.findAll('td', {'class': 'w2p_fw'}):
        cell_text = cell.text
        cell_list.append(cell_text)

    line = ';'.join(cell_list)
    final_line = line + '\n'
    outfile.write(final_line)
