import requests
from bs4 import BeautifulSoup
import time
import re

url = 'http://example.webscraping.com'
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html)
table = soup.find('table')


link_list = list()
for link in table.findAll('a'):
    country_link = url + link.get('href')
    link_list.append(country_link)

output = open('scraper_7A_file.txt', 'wb')

label_list = ['Country', 'National Flag', 'Area', 'Population', 'Iso', 'Capital', 'Continent',
              'Tld', 'Currency Code', 'Currency Name', 'Phone', 'Postal Code Format', 'Postal Code Regex',
              'Languages', 'Neighbours']
labels = ';'.join(label_list)
final_labels = labels + '\n'
output.write(final_labels)

for l in link_list:
    res = requests.get(l)
    h = res.content
    new_soup = BeautifulSoup(h)
    t = new_soup.find('table')
    time.sleep(1)

    cell_list = list()
    for row in t.findAll('tr'):
        for c in row.findAll('img'):
            image = c['src']
            image_loc = url + image
            cell_list.append(image_loc)
        for cell in row.findAll('td', {'class': 'w2p_fw'}):
            cell_text = cell.text
            if cell_text.__contains__('square kilometres'):
                re.sub('square kilometres', '', cell_text)
            cell_list.append(cell_text)

    cell_list.pop(1)
    # cell_list[0], cell_list[4] = cell_list[4], cell_list[0]
    cell_list.insert(0, cell_list.pop(4))
    line = ';'.join(cell_list)
    final_line = line + '\n'
    output.write(final_line)
