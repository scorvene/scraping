import requests
from bs4 import BeautifulSoup

url = 'http://example.webscraping.com/'
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html)
table = soup.find('table')


link_list = list()
for link in table.findAll('a'):
    country_link = url + link.get('href')
    link_list.append(country_link)

output = open('scraper_7_file.txt', 'wb')

label_list = ['National Flag:', 'Area', 'Population', 'Iso', 'Country', 'Capital', 'Continent',
              'Tld', 'Currency Code', 'Currency Name', 'Phone', 'Postal Code Format', 'Postal Code Regex',
              'Languages', 'Neighbours']
labels = ';'.join(label_list)
final_labels = labels + '\n'
output.write(final_labels)

for l in link_list:
    res = requests.get(l)
    h = res.content
    new_soup = BeautifulSoup(h)
    t = new_soup.find('table')

    cell_list = list()
    for row in t.findAll('tr'):
        for cell in row.findAll('td', {'class': 'w2p_fw'}):
            cell_text = cell.text
            cell_list.append(cell_text)

    line = ';'.join(cell_list)
    final_line = line + '\n'
    output.write(final_line)
