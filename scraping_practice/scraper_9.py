import requests
from bs4 import BeautifulSoup
import time

"""
This script scrapes the entire site of http://example.webscraping.com.
It outputs a semicolon-delimited file for import into a spreadsheet.
"""

base = 'http://example.webscraping.com/index/'

url_list = list()

for n in range(30): # works with range(26)
    ll = base + str(n)
    url_list.append(ll)

output = open('scraper_9_file_2.txt', 'wb')

label_list = ['Country', 'National Flag', 'Area', 'Population', 'Iso', 'Capital', 'Continent',
              'Tld', 'Currency Code', 'Currency Name', 'Phone', 'Postal Code Format', 'Postal Code Regex',
              'Languages', 'Neighbours']
labels = ';'.join(label_list)
final_labels = labels + '\n'
output.write(final_labels)

for url in url_list:
    response = requests.get(url)
    time.sleep(3)
    html = response.content
    soup = BeautifulSoup(html, 'lxml')
    table = soup.find('table')

    page_link_list = list()
    for link in table.findAll('a'):
        base_url = 'http://example.webscraping.com'
        country_link = base_url + link.get('href')
        page_link_list.append(country_link)

    for l in page_link_list:
        print 'processing ' + str(l)
        res = requests.get(l)
        print 'fingers tapping . . .'
        time.sleep(3)
        h = res.content
        new_soup = BeautifulSoup(h)
        t = new_soup.find('table')

        cell_list = list()
        for row in t.findAll('tr'):
            # retrieve flag image
            for c in row.findAll('img'):
                image = c['src']
                image_loc = base_url + image
                cell_list.append(image_loc)
            for cell in row.findAll('td', {'class': 'w2p_fw'}):
                cell_text = cell.text
                cell_list.append(cell_text)

        cell_list.pop(1)  # remove empty cell
        cell_list.insert(0, cell_list.pop(4))  # move country name to beginning of list
        line = ';'.join(cell_list)
        final_line = line + '\n'
        output.write(final_line)
