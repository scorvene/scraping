import requests
from bs4 import BeautifulSoup
import time

"""
This script scrapes the entire site of http://example.webscraping.com.
It outputs a semicolon-delimited file for import into a spreadsheet.
"""

base = 'http://example.webscraping.com/index/'

url_list = list()

for n in range(26):
    ll = base + str(n)
    url_list.append(ll)

output = open('scraper_8_file.txt', 'wb')

label_list = ['National Flag', 'BLANK COLUMN', 'Area', 'Population', 'Iso', 'Country', 'Capital', 'Continent',
              'Tld', 'Currency Code', 'Currency Name', 'Phone', 'Postal Code Format', 'Postal Code Regex',
              'Languages', 'Neighbours']
labels = ';'.join(label_list)
final_labels = labels + '\n'
output.write(final_labels)

for url in url_list:
    response = requests.get(url)
    time.sleep(3)
    html = response.content
    soup = BeautifulSoup(html)
    table = soup.find('table')

    page_link_list = list()
    for link in table.findAll('a'):
        base_url = 'http://example.webscraping.com'
        country_link = base_url + link.get('href')
        page_link_list.append(country_link)

    for l in page_link_list:
        print 'processing ' + str(l)

        res = requests.get(l)
        time.sleep(3)
        print 'More processing'
        h = res.content
        new_soup = BeautifulSoup(h)
        t = new_soup.find('table')

        cell_list = list()
        for row in t.findAll('tr'):
            for c in row.findAll('img'):
                image = c['src']
                image_loc = base_url + image
                cell_list.append(image_loc)
            for cell in row.findAll('td', {'class': 'w2p_fw'}):
                cell_text = cell.text
                cell_list.append(cell_text)

        line = ';'.join(cell_list)
        final_line = line + '\n'
        output.write(final_line)
