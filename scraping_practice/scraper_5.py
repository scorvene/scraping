import requests
from bs4 import BeautifulSoup

"""
This script outputs the content of all 10 pages linked to from the home page
of example.webscraping.com in XML
"""

url = 'http://example.webscraping.com/'
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html)
table = soup.find('table')

# print table.prettify()
link_list = list()
for link in table.findAll('a'):
    country_link = url + link.get('href')
    link_list. append(country_link)

country_list = list()
for row in table.findAll('td'):
    for cell in row.findAll('a'):
        country_name = cell.text
        country_list.append(country_name)

# page_list = zip(country_list, link_list)
# print page_list

output = open('scraper_5_file.xml', 'wb')
output.write('<doc>')
for l in link_list:
    res = requests.get(l)
    h = res.content
    new_soup = BeautifulSoup(h)
    t = new_soup.find('table')  # returns entirety of all pages linked to
    output.write(t.prettify())
    # t = new_soup.find_all('td', attrs={'class': 'w2p_fw'}) # returns only some elements
    # print t
output.write('</doc>')
