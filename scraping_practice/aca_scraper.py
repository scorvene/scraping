import requests
from bs4 import BeautifulSoup
import re

"""
This scraper outputs a semicolon-delimited file of data from the ACA tours page
"""

url = "https://www.adventurecycling.org/guided-tours/"
response = requests.get(url)
html = response.content

soup = BeautifulSoup(html, 'lxml')
table = soup.find("dl")

outfile = open("ACA_tours_5.txt", "wb")
titles = "Name;Type;Date;Cost;Waitlist?\n"
outfile.write(titles)

for row in table.findAll('div', attrs={'hidden-sm', 'hidden-xs'}):
    cell_list = list()
    for cell in row.findAll('a'):
        a_text = cell.text
        cell_list.append(a_text)
    for cell in row.findAll('span'):
        span_text = cell.text
        if span_text.__contains__('|'):
            span_text2 = re.sub('\|', '', span_text)
        else:
            cell_list.append(span_text)
        if span_text2.startswith(' '):
            span_text3 = span_text2.lstrip()
            cell_list.append(span_text3)
        else:
            cell_list.append(span_text2)
    if len(cell_list) > 6:
        cell_list.pop(5)
        cell_list.pop(2)
    else:
        cell_list.pop(4)
    line = ';'.join(cell_list)

    final_line = line + '\n'
    outfile.write(final_line)
