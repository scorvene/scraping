import requests
from bs4 import BeautifulSoup

url = 'http://www.hbs.edu/faculty/Pages/browse.aspx'
emeritus_url = 'http://www.hbs.edu/faculty/Pages/browse.aspx?sort=emeriti'
output = open('protwords_faculty.txt', 'wb')

response = requests.get(url)
html = response.content
soup = BeautifulSoup(html, 'lxml')

response2 = requests.get(emeritus_url)
html2 = response2.content
soup2 = BeautifulSoup(html2, 'lxml')

current_names = soup.findAll(attrs={'class': 'name'})
emeritus_names = soup2.findAll(attrs={'class': 'name'})

stopwords = ['van', 'di', 'den']
prot_list = list()

for name in current_names:
    final_list = list()
    name_stripped = name.text.strip().encode('UTF-8')
    name_list = name_stripped.split()
    for item in name_list:
        if item.find('.') == -1 and len(item) > 1 and item.lower() not in stopwords:
            final_list.append(item)
    for word in final_list:
        if word not in prot_list:
            prot_list.append(word)

for emeritus in emeritus_names:
    final_list = list()
    emeritus_stripped = emeritus.text.strip().encode('UTF-8')
    emeritus_list = emeritus_stripped.split()
    for item in emeritus_list:
        if item.find('.') == -1 and len(item) > 1 and item.lower() not in stopwords:
            final_list.append(item)
    for word in final_list:
        if word not in prot_list:
            prot_list.append(word)

really_final = '\n'.join(prot_list)
output.write(really_final)
